﻿using IdentityModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Logging;
using SPR.IdentityServer.Abstractions;
using SPR.IdentityServer.Api.Abstractions;
using SPR.IdentityServer.Api.DataContract;
using SPR.IdentityServer.Api.DataContract.Clients;
using SPR.IdentityServer.Api.DataContract.Users;
using SPR.IdentityServer.Api.Exceptions;
using SPR.IdentityServer.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Controllers.LocalApi
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [ProducesErrorResponseType(typeof(ProblemDetails))]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiExceptionFilter]
    public class ProfileController : ControllerBase
    {
        private readonly IUsersCRUDService _service;
        private readonly ILogger<ProfileController> _logger;        

        public ProfileController(IUsersCRUDService service, ILogger<ProfileController> logger)
        {
            _service = service ?? throw new ArgumentNullException(nameof(service));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        private Guid UserId => Guid.Parse(User.Claims.FirstOrDefault(i => i.Type == JwtClaimTypes.Subject)?.Value
            ?? throw new NotSupportedException("Require user"));

        /// <summary>
        /// Get user by id
        /// </summary>
        /// <returns>User info</returns>
        /// <response code="200">user was found</response>
        /// <response code="404">user does not found</response>
        [HttpGet("")]
        [Authorize(AuthPolicy.ProfilePolicy)]
        [ProducesResponseType(typeof(UserDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetAsync()
        {
            _logger.LogTrace("Request user's {UserId} profile", UserId);
            var user = await _service.GetAsync(UserId);
            if (user == null) {
                _logger.LogDebug("User with id {UserId} does not found", UserId);
                return NotFound(new ProblemDetails { Title = "User not found" });
            }

            _logger.LogDebug("User with id {UserId}, login '{Login}' is {@data}", UserId, user.Login, user);
            return Ok(user);
        }

        /// <summary>
        /// Get user's authorized clients
        /// </summary>
        /// <returns></returns>
        [HttpGet("clients")]
        [Authorize(AuthPolicy.ProfilePolicy)]
        [ProducesResponseType(typeof(ListContainer<ClientDto>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetClientsAsync()
        {
            _logger.LogTrace("Request auhtorized clients for user {UserId}", UserId);
            var clients = await _service.GetAuthorizedClientsAsync(UserId);
            _logger.LogDebug("Authorized clients for user {UserId} is {@data}", UserId, clients);
            var response = new ListContainer<ClientDto> { Items = clients };
            return Ok(response);
        }

        /// <summary>
        /// Update user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        /// <response code="404">user does not found</response>
        [HttpPost("")]
        [Authorize(AuthPolicy.ProfilePolicy)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> UpdateAsync([FromBody] UserProfileUpdateDto user)
        {
            await _service.UpdateAsync(UserId, user);
            _logger.LogDebug("Updating user's {UserId} profile: {@data}", UserId, user);
            return Ok();
        }

        /// <summary>
        /// Change password for local user
        /// </summary>
        /// <param name="request"></param>
        /// <param name="pswService"></param>
        /// <returns></returns>
        /// <response code="400">invalid old password or other request error</response>
        /// <response code="409">Can't change password for domain user</response>
        [HttpPost("password")]
        [Authorize(AuthPolicy.ProfilePolicy)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ValidationProblemDetails))]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<IActionResult> ChangePasswordAsync([FromBody] ProfileChangePasswordRequest request, [FromServices] IPasswordService pswService)
        {
            try {
                await pswService.SetPasswordAsync(UserId, request.Password, request.OldPassword);
                _logger.LogDebug("Password for user {UserId} was changed", UserId);
                return Ok();
            } catch (InvalidOldPasswordException) {
                ModelState.AddModelError("oldPassword", "Invalid old password");
                return BadRequest(new ValidationProblemDetails(ModelState));
            }
        }
    }
}

﻿using FluentAssertions;
using FluentAssertions.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.IntegrationTests
{
    public class TemplateControllerTests : BaseTests
    {
        public TemplateControllerTests(WebServiceTestFixture fixture) : base(fixture)
        {
        }

        [Theory]
        [InlineData("secret", "K7gNU3sdo+OL0wNhqoVWhr3g6s1xYv72ol/pe/Unols=")]
        public async Task HashSecretTest(string secret, string expected)
        {
            // setup

            // action
            var response = await fixture.HttpClient.GetAsync($"api/template/hash/{secret}");

            // asserts
            response.Should().Be200Ok().And.BeAs(new { hash = expected });
        }

        [Fact]
        public async Task GetTemplateConfigurationListTest()
        {
            // setup

            // action
            var response = await fixture.HttpClient.GetAsync($"api/template/configuration");

            // asserts
            response.Should().Be200Ok();
            var body = await response.Content.ReadAsStringAsync();
            var jt = JToken.Parse(body);
            jt.Should().HaveElement("items").Which.Should().BeOfType<JArray>().Which.Select(i => (string)i).Should().Contain("worker");
        }

        [Theory]
        [InlineData("worker")]
        public async Task GetTemplateConfigurationTest(string name)
        {
            // setup

            // action
            var response = await fixture.HttpClient.GetAsync($"api/template/configuration/{name}");

            // asserts
            response.Should().Be200Ok();
            var body = await response.Content.ReadAsStringAsync();
            body.Should().NotBeNullOrEmpty();
        }
    }
}

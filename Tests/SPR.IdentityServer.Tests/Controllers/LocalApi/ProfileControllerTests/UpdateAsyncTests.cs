﻿using AutoFixture.Xunit2;
using FluentAssertions;
using FluentAssertions.AspNetCore.Mvc;
using Moq;
using SPR.IdentityServer.Api.DataContract.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Tests.Controllers.LocalApi.ProfileControllerTests
{
    public class UpdateAsyncTests : ProfileControllerBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(UserProfileUpdateDto user)
        {
            // setup
            //usersMock.Setup(f => f.UpdateAsync(user)).Ret

            // action
            var result = await controller.UpdateAsync(user);

            // asserts
            result.Should().BeOkResult();
            usersMock.Verify(f => f.UpdateAsync(userId, user), Times.Once);
        }
    }
}

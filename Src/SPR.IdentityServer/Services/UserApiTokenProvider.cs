﻿using IdentityServer4;
using Microsoft.Extensions.Logging;
using SPR.IdentityServer.Abstractions;
using System;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Services
{
    public class UserApiTokenProvider : IUserApiTokenProvider
    {
        private readonly IdentityServerTools identityServerTools;
        private readonly ILogger<UserApiTokenProvider> logger;

        private const int ExpiresIn = 60 * 15;

        public UserApiTokenProvider(IdentityServerTools identityServerTools, ILogger<UserApiTokenProvider> logger)
        {
            this.identityServerTools = identityServerTools ?? throw new ArgumentNullException(nameof(identityServerTools));
            this.logger = logger;
        }

        public Task<string> GetTokenAsync()
        {
            logger?.LogDebug("Issuing token for UsersStore");

            // TODO: read token in integration test
            // https://stackoverflow.com/questions/38340078/how-to-decode-jwt-token
            //
            // issue tokens
            // https://www.strathweb.com/2017/10/self-issuing-an-identityserver4-token-in-an-identityserver4-service/
            return identityServerTools.IssueClientJwtAsync(
                "IdentityServer", ExpiresIn,
                new[] { "users_store_api.login" },
                new[] { "users_store_api" });            
        }
    }
}

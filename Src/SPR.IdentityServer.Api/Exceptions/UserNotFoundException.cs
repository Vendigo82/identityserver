﻿using System;

namespace SPR.IdentityServer.Api.Exceptions
{
    public class UserNotFoundException : Exception
    {
        public UserNotFoundException(Guid userId) : base($"User with id {userId} does not found")
        {
        }
    }
}

﻿using AutoFixture.Xunit2;
using FluentAssertions.AspNetCore.Mvc;
using Moq;
using SPR.IdentityServer.Api.DataContract.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Controllers.LocalApi.UsersControllerTests
{
    public class GetBunchAsyncTests : UsersControllerBaseTests
    {
        [Theory, AutoData]
        public async Task ShouldOk(IEnumerable<UserDto> users)
        {
            // setup
            serviceMock.Setup(f => f.GetListAsync(It.IsAny<IEnumerable<Guid>>())).ReturnsAsync(users);

            // action
            var result = await controller.GetBunchAsync(users.Select(i => i.Id));

            // asserts
            result.Should().BeOkObjectResult().WithValueEquivalentTo(new { Items = users });

            serviceMock.Verify(f => f.GetListAsync(It.Is<IEnumerable<Guid>>(m => Enumerable.SequenceEqual(m, users.Select(i => i.Id)))), Times.Once);
            serviceMock.VerifyNoOtherCalls();
        }

        [Fact]
        public async Task EmptyRequestShouldNotCallService()
        {
            // setup

            // action
            var result = await controller.GetBunchAsync(Enumerable.Empty<Guid>());

            // asserts
            result.Should().BeOkObjectResult().WithValueEquivalentTo(new { Items = Enumerable.Empty<Guid>() });

            serviceMock.VerifyNoOtherCalls();
        }
    }
}

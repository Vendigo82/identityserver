﻿using AutoFixture.Xunit2;
using FluentAssertions.AspNetCore.Mvc;
using Moq;
using SPR.IdentityServer.Api.DataContract.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Controllers.LocalApi.ClientControllerTests
{
    public class UpdateTests : ClientControllerBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(ClientDto client)
        {
            // setup
            
            // asserts
            var response = await controller.UpdateAsync(client);

            // asserts
            response.Should().BeOkResult();

            clientsMock.Verify(f => f.UpdateAsync(client), Times.Once);
            clientsMock.VerifyNoOtherCalls();
        }

        [Theory, AutoData]
        public async Task BadRequestTest(ClientDto client)
        {
            // setup
            client.Id = Guid.Empty;

            // asserts
            var response = await controller.UpdateAsync(client);

            // asserts
            response.Should().BeBadRequestObjectResult();

            clientsMock.VerifyNoOtherCalls();
        }
    }
}

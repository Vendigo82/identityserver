﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Api.DataContract.Users
{
    public class UpdateUserClientsRequest
    {
        /// <summary>
        /// List of client's id which access should be revoked
        /// </summary>        
        public IEnumerable<Guid>? Revoke { get; set; }

        /// <summary>
        /// List of client's id which access should be granted
        /// </summary>
        public IEnumerable<Guid>? Grant { get; set; }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using SPR.IdentityServer.Abstractions.Auth;
using SPR.IdentityServer.Api.Abstractions;
using SPR.IdentityServer.Api.Exceptions;
using SPR.IdentityServer.DAL.Model;
using System;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Api.Services
{
    public class PasswordService : IPasswordService
    {
        private readonly UsersContext _context;
        private readonly IPasswordHasher _hasher;
        private readonly ISaltGenerator _saltGenerator;
        private readonly IPasswordValidator _passwordValidator;

        public PasswordService(UsersContext context, IPasswordHasher hasher, ISaltGenerator saltGenerator, IPasswordValidator passwordValidator)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _hasher = hasher ?? throw new ArgumentNullException(nameof(hasher));
            _saltGenerator = saltGenerator ?? throw new ArgumentNullException(nameof(saltGenerator));
            _passwordValidator = passwordValidator ?? throw new ArgumentNullException(nameof(passwordValidator));
        }

        public Task SetPasswordAsync(Guid userId, string? password)
            => SetPasswordAsync(userId, password, false, null);

        public Task SetPasswordAsync(Guid userId, string? password, string? oldPassword)
            => SetPasswordAsync(userId, password, true, oldPassword);

        private async Task SetPasswordAsync(Guid userId, string? password, bool verifyOldPassword, string? oldPassword)
        {
            var user = await _context.Users.FirstOrDefaultAsync(i => i.Id == userId);
            if (user == null)
                throw new UserNotFoundException(userId);

            if (user.LoginTypeId == (short)Enums.LoginTypes.Domain)
                throw new ChangePasswordViolationException("Can't change password for domain user");

            if (verifyOldPassword) {
                if (!_passwordValidator.Validate(user.Salt, user.PasswordHash, oldPassword)) {
                    throw new InvalidOldPasswordException();
                }
            }

            if (string.IsNullOrEmpty(password)) {
                user.Salt = null;
                user.PasswordHash = null;
            } else {
                user.Salt = _saltGenerator.GenSalt();
                user.PasswordHash = _hasher.HashPassword(user.Salt, password);
            }

            await _context.SaveChangesAsync();
        }
    }
}

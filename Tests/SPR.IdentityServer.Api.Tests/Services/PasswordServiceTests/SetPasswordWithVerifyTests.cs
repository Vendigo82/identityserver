﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Moq;
using SPR.IdentityServer.Api.Exceptions;
using SPR.IdentityServer.Api.Services.PasswordServiceTests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Api.Services.PasswordServiceTests
{
    public class SetPasswordWithVerifyTests : PasswordServiceBaseTests
    {
        public SetPasswordWithVerifyTests()
        {
            context.Users.Add(new DAL.Model.User {
                Id = Guid.NewGuid(),
                Salt = "oldsalt",
                PasswordHash = "oldhash",
                LoginTypeId = (short)Enums.LoginTypes.Local
            });
            context.SaveChangesAsync();
        }

        [Theory, AutoData]
        public async Task SuccessTest(string password, string salt, string hash, string oldPassword)
        {
            // setup
            var dbuser = context.Users.First();
            saltMock.Setup(f => f.GenSalt()).Returns(salt);
            hasherMock.Setup(f => f.HashPassword(salt, password)).Returns(hash);
            passwordValidatorMock.Setup(f => f.Validate("oldsalt", "oldhash", oldPassword)).Returns(true);

            // action
            await service.SetPasswordAsync(dbuser.Id, password, oldPassword);

            // asserts
            saltMock.Verify(f => f.GenSalt(), Times.Once);
            hasherMock.Verify(f => f.HashPassword(salt, password), Times.Once);
            passwordValidatorMock.Verify(f => f.Validate("oldsalt", "oldhash", oldPassword), Times.Once);
            mockRepository.VerifyNoOtherCalls();

            dbuser.Should().BeEquivalentTo(new {
                Salt = salt,
                PasswordHash = hash
            });
        }

        [Theory, AutoData]
        public async Task OldPasswordFailedTest(string password, string oldPassword)
        {
            // setup
            var dbuser = context.Users.First();
            passwordValidatorMock.Setup(f => f.Validate("oldsalt", "oldhash", oldPassword)).Returns(false);

            // action
            Func<Task> action = () => service.SetPasswordAsync(dbuser.Id, password, oldPassword);

            // asserts
            await action.Should().ThrowExactlyAsync<InvalidOldPasswordException>();

            passwordValidatorMock.Verify(f => f.Validate("oldsalt", "oldhash", oldPassword), Times.Once);
            mockRepository.VerifyNoOtherCalls();

            dbuser.Should().BeEquivalentTo(new {
                Salt = "oldsalt",
                PasswordHash = "oldhash"
            });
        }

        [Theory]
        [InlineAutoData("")]
        [InlineAutoData(null)]
        public async Task EmptyPassword(string password, string oldPassword)
        {
            // setup
            var dbuser = context.Users.First();
            passwordValidatorMock.Setup(f => f.Validate("oldsalt", "oldhash", oldPassword)).Returns(true);

            // action
            await service.SetPasswordAsync(dbuser.Id, password, oldPassword);

            // asserts
            passwordValidatorMock.Verify(f => f.Validate("oldsalt", "oldhash", oldPassword), Times.Once);
            mockRepository.VerifyNoOtherCalls();

            dbuser.Salt.Should().BeNull();
            dbuser.PasswordHash.Should().BeNull();
        }

        [Fact]
        public async Task UserNotFoundTest()
        {
            // setup

            // action
            Func<Task> action = () => service.SetPasswordAsync(Guid.NewGuid(), "psw");

            // asserts
            await action.Should().ThrowExactlyAsync<UserNotFoundException>();
        }

        [Fact]
        public async Task DomainUserTest()
        {
            // setup
            var dbuser = context.Users.First();
            dbuser.LoginTypeId = (short)Enums.LoginTypes.Domain;
            context.SaveChanges();

            // action
            Func<Task> action = () => service.SetPasswordAsync(dbuser.Id, "psw");

            // asserts
            await action.Should().ThrowExactlyAsync<ChangePasswordViolationException>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Api.Exceptions
{
    public class ClientNotFoundException : Exception
    {
        public ClientNotFoundException(Guid clientId) : base($"Client with id={clientId} does not found")
        {
        }
    }
}

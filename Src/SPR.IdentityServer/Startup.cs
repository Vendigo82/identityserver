using HealthChecks.UI.Client;
using Hellang.Middleware.ProblemDetails;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Converters;
using SPR.IdentityServer.Abstractions;
using SPR.IdentityServer.Abstractions.Auth;
using SPR.IdentityServer.Api.Abstractions;
using SPR.IdentityServer.Api.Mapping;
using SPR.IdentityServer.Api.Services;
using SPR.IdentityServer.Certificates;
using SPR.IdentityServer.DAL.Model;
using SPR.IdentityServer.Services;
using SPR.IdentityServer.Services.Auth;
using SPR.IdentityServer.Services.Mapping;
using System;
using System.IO;
using System.Reflection;

namespace SPR.IdentityServer
{
    public class Startup
    {
        public Startup(IWebHostEnvironment host, IConfiguration configuration)
        {
            Configuration = configuration;
            Host = host;
        }

        public IConfiguration Configuration { get; }

        public IWebHostEnvironment Host { get; }


        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            // Configuration section name
            const string iscfg = "IdentityServer";

            var persistedMigrationAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;

            services.AddControllersWithViews().AddNewtonsoftJson(options => {
                options.SerializerSettings.Converters.Add(new StringEnumConverter());
            });

            //services.Configure<RewriteIdetityServerUrlMiddleware.Settings>(Configuration.GetSection("IdentityServerUrl"));

            services.AddProblemDetails(options => {
                options.IncludeExceptionDetails = (c, e) => Host.IsDevelopment();
            });

            AddSwagger();

            services.AddDbContext<UsersContext>(options => options.UseNpgsql(Configuration.GetConnectionString("Users")));

            //services.AddCorrelationId();

            //configure identity server services
            AddIdentityServer();

            AddHealthCheck();

            services.AddAuthentication().AddLocalApi();
            services.AddAuthorization(options => {
                options.ConfigureLocalApiPolicies();
                });

            services.AddAutoMapper(typeof(EFMappingProfile), typeof(MappingProfile));

            services.AddTransient<IUserApiTokenProvider, UserApiTokenProvider>();
            services.AddTransient<IProfileService, UsersProfileService>();
            services.AddTransient<IPasswordHasher, SHA256PasswordHasher>();
            services.AddTransient<IPasswordValidator, PasswordValidator>();
            services.AddTransient<IUsersProvider, DatabaseUsersProvider>();
            services.AddTransient<IUserCredentialsValidator, CredentialsValidator>();

            //local api dependencies
            services.AddTransient<ISaltGenerator, SaltGenerator>();

            services.AddTransient<IClientsCRUDService, ClientsCRUDService>();
            services.AddTransient<IUsersCRUDService, UsersCRUDService>();
            services.AddTransient<IPasswordService, PasswordService>();

            //services
            //    .AddHttpClient<IUsersApiClient, UsersApiClient>((sp, client) => {
            //        client.BaseAddress = new Uri("http://localhost:5002");   // TODO: from settings
            //    }).AddUserApiTokenHandler();

            void AddSwagger()
            {
                services.AddSwaggerGen(c => {
                    c.SwaggerDoc("v1", new OpenApiInfo { Title = "SPR.IdentityServer.Api", Version = "v1" });

                    c.SupportNonNullableReferenceTypes();
                    c.UseAllOfToExtendReferenceSchemas();

                    // Set the comments path for the Swagger JSON and UI.
                    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                    c.IncludeXmlComments(xmlPath);

                    var apiXmlFile = $"{typeof(SPR.IdentityServer.Api.DataContract.ObjectIdDto).Assembly.GetName().Name}.xml";
                    c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, apiXmlFile));
                });
                services.AddSwaggerGenNewtonsoftSupport();
            }

            void AddIdentityServer()
            {
                var builder = services
                    .AddIdentityServer(options => {
                        options.UserInteraction.LoginUrl = "/Account/Login";
                        options.UserInteraction.LogoutUrl = "/Account/Logout";

                        options.Discovery.CustomEntries.Add("clients_api", "~/api/clients");
                        options.Discovery.CustomEntries.Add("users_api", "~/api/users");
                    })
                    .AddInMemoryIdentityResources(Configuration.GetSection(string.Concat(iscfg, ":IdentityResources")))
                    .AddInMemoryApiScopes(Configuration.GetSection(string.Concat(iscfg, ":ApiScopes")))
                    .AddInMemoryApiResources(Configuration.GetSection(string.Concat(iscfg, ":ApiResources")))
                    //.AddInMemoryClients(Configuration.GetSection(string.Concat(iscfg, ":Clients")))
                    .AddClientStore<ClientStore>()

                    .AddOperationalStore(options => {
                        options.ConfigureDbContext = b => b.UseNpgsql(
                            Configuration.GetConnectionString("PersistedGrand"),
                            sql => sql.MigrationsAssembly(persistedMigrationAssembly));
                    })
                    .AddProfileService<UsersProfileService>()
                    .AddSigninCredentialFromConfig(Host, Configuration.GetSection("SigninKeyCredentials"))
                    .AddResourceOwnerValidator<ResourceOwnerPasswordValidator>()
                    ;

               //builder.AddDeveloperSigningCredential();

                // Configure SameSiteCookies which broken in Chrome 80
                // Need same configuration in every IS client
                services.ConfigureNonBreakingSameSiteCookies();
            }

            void AddHealthCheck()
            {
                services.AddHealthChecks()
                    .AddDbContextCheck<UsersContext>("Database", tags: new[] { "database" })
                    .AddDbContextCheck<PersistedGrantDbContext>("PersistedGrandDb", tags: new[] { "database" });
            }
        }
        
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            InitializeDatabase(app);

            app.UseCookiePolicy();
            //app.UseCorrelationId();

            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }
            app.UseWhen(c => c.Request.Path.StartsWithSegments("/api"), b => b.UseProblemDetails());

            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "SPR.UsersStore.Api v1"));

            app.UseHealthChecks("/health", new HealthCheckOptions {
                ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
            });

            app.UseStaticFiles();
            app.UseRouting();

            //app.UseRewriteIdetityServerUrl();
            app.UseIdentityServer();

            app.UseAuthorization();
             
            app.UseEndpoints(endpoints => {
                endpoints.MapDefaultControllerRoute();
            });
        }

        private static void InitializeDatabase(IApplicationBuilder app)
        {
            using var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope(); 
            serviceScope.ServiceProvider.GetRequiredService<PersistedGrantDbContext>().Database.Migrate();
            //serviceScope.ServiceProvider.GetRequiredService<UsersContext>().Database.Migrate();
        }
    }
}

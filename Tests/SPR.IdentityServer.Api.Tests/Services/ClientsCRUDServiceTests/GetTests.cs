﻿using AutoFixture;
using FluentAssertions;
using SPR.IdentityServer.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Api.Services.ClientsCRUDServiceTests
{
    public class GetTests : ClientsCRUDServiceBaseTests
    {
        public GetTests()
        {
            context.Clients.AddRange(Enumerable.Range(1, 2).Select(i => fixture.Build<Client>().Without(i => i.UserClients).Create()));
            context.SaveChanges();
        }

        [Fact]
        public async Task SuccessTest()
        {
            // setup
            var dbitem = context.Clients.First();

            // action
            var result = await service.GetAsync(dbitem.Id);

            // asserts
            result.Should().NotBeNull();
            dbitem.Should().BeEquivalentTo(result);
        }

        [Fact]
        public async Task NotFoundTest()
        {
            // setup

            // action
            var result = await service.GetAsync(Guid.NewGuid());

            // asserts
            result.Should().BeNull();
        }
    }
}

﻿using Microsoft.Extensions.DependencyInjection;
using SPR.IdentityServer.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Services.HttpClientHandlers
{
    /// <summary>
    /// Add authorization token from <see cref="IUserApiTokenProvider"/>
    /// </summary>
    public class UserApiTokenDelegatingHandler : DelegatingHandler
    {
        private readonly IUserApiTokenProvider tokenProvider;

        public UserApiTokenDelegatingHandler(IUserApiTokenProvider tokenProvider)
        {
            this.tokenProvider = tokenProvider ?? throw new ArgumentNullException(nameof(tokenProvider));
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", tokenProvider.GetTokenAsync().Result);
            return base.SendAsync(request, cancellationToken);
        }
    }

    public static class UserApiTokenDelegatingHandlerExtensions
    {
        /// <summary>
        /// Add authorization token from <see cref="IUserApiTokenProvider"/> for HttpClient
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static IHttpClientBuilder AddUserApiTokenHandler(this IHttpClientBuilder builder)
        {
            builder.Services.AddTransient<UserApiTokenDelegatingHandler>();
            return builder.AddHttpMessageHandler<UserApiTokenDelegatingHandler>();
        }
    }
}

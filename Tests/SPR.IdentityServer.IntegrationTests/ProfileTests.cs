﻿using AutoFixture.Xunit2;
using FluentAssertions;
using FluentAssertions.Json;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Linq;
using SPR.IdentityServer.DAL.Model;
using SPR.IdentityServer.Enums;
using SPR.IdentityServer.IntegrationTests.TestHost;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.IntegrationTests
{
    public class ProfileTests : BaseTests
    {
        private readonly ClaimsFactory claims;

        public ProfileTests(WebServiceTestFixture fixture) : base(fixture)
        {
            claims = fixture.ServiceProvider.GetRequiredService<ClaimsFactory>();
        }

        [Fact]
        public async Task GetTest()
        {
            // setup
            var dbuser = Context.Users.First(i => i.IsDisabled == false);
            claims.UserId = dbuser.Id;

            // action
            var response = await fixture.HttpClient.GetAsync("api/profile");

            // response
            response.StatusCode.Should().Be(System.Net.HttpStatusCode.OK);
            var token = JToken.Parse(await response.Content.ReadAsStringAsync());
            dbuser.Should().BeEquivalentTo(new {
                Id = (Guid)token["id"],
                Name = (string)token["name"],
                Login = (string)token["login"],
                LoginTypeId = (int)Enum.Parse<LoginTypes>((string)token["loginType"])
            });            
        }

        [Fact]
        public async Task GetClientsTest()
        {
            // setup
            var dbuser = Context.Users
                .Include(p => p.UserClients)
                .ThenInclude(i => i.Client)
                .First(i => i.IsDisabled == false && i.UserClients.Any());
            claims.UserId = dbuser.Id;

            // action
            var response = await fixture.HttpClient.GetFromJsonAsync<Api.DataContract.ListContainer<Api.DataContract.Clients.ClientDto>>("api/profile/clients");

            // response
            response.Should().BeEquivalentTo(new { 
                Items = dbuser.UserClients.Select(i => new { 
                    Id = i.Client.Id,
                    SystemName = i.Client.SystemName,
                    Title = i.Client.Title,
                    IsDisabled = i.Client.IsDisabled
                })
            });
        }

        [Theory, AutoData]
        public async Task ChangePasswordTest(string password)
        {
            // setup
            var dbuser = new User { Login = Guid.NewGuid().ToString(), LoginTypeId = 1 };
            Context.Users.Add(dbuser);
            await Context.SaveChangesAsync();
            createdUsers.Add(dbuser.Id);

            claims.UserId = dbuser.Id;

            // asserts
            var request = new {
                oldPassword = "",
                password
            };

            var response = await fixture.HttpClient.PostAsJsonAsync("api/profile/password", request);

            // asserts
            response.StatusCode.Should().Be(System.Net.HttpStatusCode.OK);

            await Context.Entry(dbuser).ReloadAsync();
            dbuser.Salt.Should().NotBeNullOrWhiteSpace();
            dbuser.PasswordHash.Should().NotBeNullOrWhiteSpace();
        }

        [Theory, AutoData]
        public async Task UpdateTest(string name)
        {
            // setup
            var dbuser = new User { Login = Guid.NewGuid().ToString(), LoginTypeId = 1 };
            Context.Users.Add(dbuser);
            await Context.SaveChangesAsync();
            createdUsers.Add(dbuser.Id);

            claims.UserId = dbuser.Id;

            // asserts
            var request = new {
                id = dbuser.Id,
                name = name
            };

            var response = await fixture.HttpClient.PostAsJsonAsync("api/profile", request);

            // asserts
            response.StatusCode.Should().Be(System.Net.HttpStatusCode.OK);

            await Context.Entry(dbuser).ReloadAsync();
            dbuser.Name.Should().Be(name);
        }
    }
}

﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SPR.IdentityServer.DAL.Model
{
    public partial class LoginType
    {
        public LoginType()
        {
            Users = new HashSet<User>();
        }

        public short Id { get; set; }
        public string SystemName { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}

﻿using SPR.IdentityServer.Abstractions.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Services.Auth
{
    public class SHA256PasswordHasher : IPasswordHasher
    {
        public string HashPassword(string salt, string password)
        {
            using var hash = SHA256.Create();

            var hashBytes = hash.ComputeHash(Encoding.UTF8.GetBytes(string.Concat(salt, password)));

            // Convert the byte array to hexadecimal string
            StringBuilder sb = new();
            for (int i = 0; i < hashBytes.Length; i++) {
                sb.Append(hashBytes[i].ToString("x2"));
            }

            return sb.ToString();
        }
    }
}

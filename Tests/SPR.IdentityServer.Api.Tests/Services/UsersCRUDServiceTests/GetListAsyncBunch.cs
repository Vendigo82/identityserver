﻿using AutoFixture;
using FluentAssertions;
using SPR.IdentityServer.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Api.Services.UsersCRUDServiceTests
{
    public class GetListAsyncBunch : UsersCRUDServiceBaseTests
    {
        public GetListAsyncBunch()
        {
            context.Users.AddRange(Enumerable.Range(1, 10).Select(i => fixture.Create<User>()));
            context.SaveChanges();
        }

        [Fact]
        public async Task SuccessTest()
        {
            // setup
            var dbusers = context.Users.Take(3).ToList();

            // action
            var result = await service.GetListAsync(dbusers.Select(i => i.Id));

            // asserts
            result.Should().Equal(dbusers, (i, j) => i.Id == j.Id);
        }
    }
}

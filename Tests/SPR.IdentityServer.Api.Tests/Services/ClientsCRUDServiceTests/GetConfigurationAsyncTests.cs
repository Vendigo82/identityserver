﻿using AutoFixture.Xunit2;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Api.Services.ClientsCRUDServiceTests
{
    public class GetConfigurationAsyncTests : ClientsCRUDServiceBaseTests
    {
        [Theory, AutoData]
        public async Task GetClientWithConfigurationTest(Guid clientId)
        {
            // setup
            var dbitem = context.Clients.Add(new DAL.Model.Client {
                Id = clientId,
                Configuration = "{ \"field\": 123 }",
            });
            await context.SaveChangesAsync();

            // action
            var result = await service.GetConfigurationAsync(clientId);

            // asserts
            ((object)result).Should().NotBeNull();
            result.ToString().Should().Be(dbitem.Entity.Configuration);
        }

        [Theory, AutoData]
        public async Task GetClientWithoutConfiguration(Guid clientId)
        {
            // setup
            context.Clients.Add(new DAL.Model.Client {
                Id = clientId,
            });
            await context.SaveChangesAsync();

            // action
            var result = await service.GetConfigurationAsync(clientId);

            // asserts
            ((object)result).Should().BeNull();
        }

        [Theory, AutoData]
        public async Task ClientNotFoundTest(Guid clientId)
        {
            // setup

            // action
            Func<Task> action = () => service.GetConfigurationAsync(clientId);

            // asserts
            await action.Should().ThrowExactlyAsync<Exceptions.ClientNotFoundException>();
        }
    }
}

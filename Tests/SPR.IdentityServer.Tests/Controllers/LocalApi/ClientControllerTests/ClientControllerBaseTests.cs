﻿using Microsoft.Extensions.Logging;
using Moq;
using SPR.IdentityServer.Api.Abstractions;
using SPR.IdentityServer.Controllers.LocalApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Controllers.LocalApi.ClientControllerTests
{
    public class ClientControllerBaseTests
    {
        protected readonly Mock<IClientsCRUDService> clientsMock = new();
        protected readonly ClientsController controller;

        public ClientControllerBaseTests()
        {
            var logger = new Mock<ILogger<ClientsController>>();
            controller = new ClientsController(clientsMock.Object, logger.Object);
        }
    }
}

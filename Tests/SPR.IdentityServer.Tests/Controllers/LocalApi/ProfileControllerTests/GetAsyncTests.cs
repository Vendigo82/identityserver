﻿using AutoFixture.Xunit2;
using FluentAssertions.AspNetCore.Mvc;
using Moq;
using SPR.IdentityServer.Api.DataContract.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Tests.Controllers.LocalApi.ProfileControllerTests
{
    public class GetAsyncTests : ProfileControllerBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(UserDto userDto)
        {
            // setup
            usersMock.Setup(f => f.GetAsync(userId)).ReturnsAsync(userDto);

            // action
            var result = await controller.GetAsync();

            // asserts
            result.Should().BeOkObjectResult().WithValueEquivalentTo(userDto);
            usersMock.Verify(f => f.GetAsync(userId), Times.Once);
        }
    }
}

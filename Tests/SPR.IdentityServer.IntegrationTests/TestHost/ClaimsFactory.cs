﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.IdentityServer.IntegrationTests.TestHost
{
    public class ClaimsFactory
    {
        public Guid UserId { get; set; } = Guid.NewGuid();
    }
}

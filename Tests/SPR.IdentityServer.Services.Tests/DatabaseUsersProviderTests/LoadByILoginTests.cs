﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Services.DatabaseUsersProviderTests
{
    public class LoadByLoginTests : DatabaseUsersProviderBaseTests
    {
        [Fact]
        public async Task SuccessTest()
        {
            // setup
            var dbitem = context.Users.First();

            // action
            var result = await store.LoadAsync(dbitem.Login);

            // asserts
            dbitem.Should().NotBeNull().And.BeEquivalentTo(result, o => o.Excluding(p => p.LoginType));
            result.LoginType.Should().Be((Enums.LoginTypes)dbitem.LoginTypeId);
        }

        [Fact]
        public async Task NotFoundTest()
        {
            // setup

            // action
            var result = await store.LoadAsync("unknown");

            // asserts
            result.Should().BeNull();
        }

        [Fact]
        public async Task DisableUserTest()
        {
            // setup
            var dbitem = context.Users.First();
            dbitem.IsDisabled = true;
            context.SaveChanges();

            // action
            var result = await store.LoadAsync(dbitem.Login);

            // asserts
            result.Should().BeNull();
        }
    }
}

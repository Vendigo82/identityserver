-- 

CREATE EXTENSION IF NOT EXISTS "pgcrypto";

-- --- System procedures

create schema if not exists utils;

create or replace function utils.execute(text)
    returns void
    language PlPgSQL strict as $$
begin
    execute $1;
end
$$;

create or replace function utils.table_exists(_schema_name text, _table_name text)
    returns bool
    language SQL strict
as $$
    select exists(
        select 1
        from pg_tables
        where schemaname = _schema_name
            and tablename = _table_name
    );
$$;

create or replace function utils.index_exists(_schema_name text, _table_name text, _index_name text)
    returns bool
    language SQL strict
as $$
    select exists(
        select 1
        from pg_indexes
        where schemaname = _schema_name
            and tablename = _table_name
            and indexname = _index_name
    );
$$;

create or replace function utils.column_exists(_schema_name text, _table_name text, _column_name text)
    returns bool
    language SQL strict
as $$
    select exists(
        select 1
        from pg_attribute a, pg_type t, pg_class c, pg_namespace ns
        where a.attnum > 0 -- The number of the column. Ordinary columns are numbered from 1 up. System columns, such as oid, have (arbitrary) negative numbers.
            and a.atttypid = t.oid -- The data type of this column
            and a.attrelid = c.oid -- The table this column belongs to
            and c.relnamespace = ns.oid -- The OID of the namespace that contains this relation
            and ns.nspname = _schema_name
            and c.relname = _table_name
            and a.attname = _column_name -- The column name
    );
$$;

create or replace function utils.fk_exists(_schema_name text, _table_name text, _fk_name text)
    returns bool
    language SQL strict
as $$
    select exists(
        select 1
        from pg_constraint a, pg_namespace ns, pg_class c
        where a.contype = 'f'
            and a.conrelid = c.oid -- The table this column belongs to
            and a.connamespace = ns.oid -- The OID of the namespace that contains this relation
            and ns.nspname = _schema_name
            and c.relname = _table_name
            and a.conname = _fk_name
    );
$$;

create or replace function utils.pk_def_exists(
    _schema_name text,
    _table_name text,
    _idx_def_array text[] -- like ARRAY['1:field1', '2:field2', '3:field3']
)
    returns bool
    -- language PlPgSQL VOLATILE
    language PlPgSQL strict as $$
declare
    _cnt integer;
begin
    select count(*)
        into _cnt
    from (
        select a.attname,
            ( -- Position of the field in _index_. No field DDT index.
                select tmpx2.i + 1
                from (
                    SELECT generate_series(array_lower(i.indkey, 1), array_upper(i.indkey, 1)) as i
                ) tmpx2
                where i.indkey[i] = a.attnum
            ) as posicao
        from pg_class c, pg_namespace ns, pg_index i, pg_attribute a
        where
            c.oid = _table_name::regclass
            and ns.oid = c.relnamespace
            and ns.nspname = _schema_name
            and i.indrelid = c.oid
            and i.indisprimary
            and a.attrelid = c.oid
            and a.attnum = any(i.indkey)
    ) tmp
    where (tmp.posicao || ':' || attname::text) in (select * from unnest(_idx_def_array))
    ;
    return array_length(_idx_def_array, 1) = _cnt;
end
$$;

create or replace function utils.drop_function(_schema_name text, _func_name text)
    returns text
    language PlPgSQL volatile as $$
declare
    _row record;
    _dropped_count smallint := 0;
    _params_count int;
    _func_proto text;
    _i int;
begin
    for _row in (
        select p.proargtypes
        from pg_proc p, pg_namespace ns
        where p.pronamespace = ns.oid
            and ns.nspname = _schema_name
            and p.proname = _func_name
    ) loop
        _params_count = array_upper(_row.proargtypes, 1) + 1;

        _func_proto = _schema_name || '.' ||_func_name || '(';
        _i = 0;
        while _i < _params_count loop
            if _i > 0 then
                _func_proto = _func_proto || ', ';
            end if;
            _func_proto = _func_proto || (select typname from pg_type where oid = _row.proargtypes[_i]);
            _i = _i + 1;
        end loop;
        _func_proto = _func_proto || ');';

        execute 'drop function ' || _func_proto;
        _dropped_count = _dropped_count + 1;
    end loop;
    return 'Dropped ' || _dropped_count || ' functions with name ' || _schema_name || '.' || _func_name;
END;
$$;

-- ---

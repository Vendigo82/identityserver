﻿using AutoFixture.Xunit2;
using FluentAssertions;
using FluentAssertions.AspNetCore.Mvc;
using Moq;
using SPR.IdentityServer.Api.DataContract.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Controllers.LocalApi.UsersControllerTests
{
    public class UpdateTests : UsersControllerBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(UserDto user)
        {
            // setup

            // action
            var result =  await controller.UpdateUserAsync(user);

            // asserts
            result.Should().BeOkResult();
            serviceMock.Verify(f => f.UpdateAsync(user), Times.Once);            
        }

        [Theory, AutoData]
        public async Task BadRequestTest(UserDto user)
        {
            // setup
            user.Id = Guid.Empty;

            // action
            var result = await controller.UpdateUserAsync(user);

            // asserts
            result.Should().BeBadRequestObjectResult();            
        }
    }
}

﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using SPR.IdentityServer.Abstractions;
using SPR.IdentityServer.DAL.Model;
using SPR.IdentityServer.DataModel;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Services
{
    public class DatabaseUsersProvider : IUsersProvider
    {
        private readonly UsersContext context;
        private readonly IMapper mapper;

        public DatabaseUsersProvider(UsersContext context, IMapper mapper)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public Task<UserModel?> LoadAsync(Guid id)
        {
            return context
                .Users
                .AsNoTracking()
                .Where(i => i.Id == id && i.IsDisabled == false)
                .ProjectTo<UserModel?>(mapper.ConfigurationProvider)
                .SingleOrDefaultAsync();
        }

        public Task<UserModel?> LoadAsync(string login)
        {
            return context
                .Users
                .AsNoTracking()
                .Where(i => i.Login == login && i.IsDisabled == false)
                .ProjectTo<UserModel?>(mapper.ConfigurationProvider)
                .SingleOrDefaultAsync();
        }

        public async Task<bool> CanLoginAsync(Guid userId, string? clientName)
        {
            var existsUser = await context.Users.AnyAsync(i => i.Id == userId && i.IsDisabled == false);
            if (!existsUser)
                return false;

            if (clientName == null)
                return true;

            return await context
                .Clients
                .AsNoTracking()
                .Where(i => i.SystemName == clientName && i.IsDisabled == false 
                    && (i.RequirePermission == false || i.UserClients.Any(u => u.UserId == userId))
                    )
                .AnyAsync();
        }
    }
}

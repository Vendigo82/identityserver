﻿using Newtonsoft.Json;
using System;

namespace SPR.IdentityServer.Api.DataContract.Users
{
    public class UserProfileUpdateDto
    {
        /// <summary>
        /// User's name
        /// </summary>        
        [JsonProperty(Required = Required.AllowNull)]
        public string? Name { get; set; }
    }
}

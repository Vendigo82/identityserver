﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SPR.IdentityServer.DAL.Model
{
    public partial class Client
    {
        public Client()
        {
            UserClients = new HashSet<UserClient>();
        }

        public Guid Id { get; set; }
        public string SystemName { get; set; }
        public bool IsDisabled { get; set; }
        public string Title { get; set; }
        public bool? RequirePermission { get; set; }
        public string Configuration { get; set; }

        public virtual ICollection<UserClient> UserClients { get; set; }
    }
}

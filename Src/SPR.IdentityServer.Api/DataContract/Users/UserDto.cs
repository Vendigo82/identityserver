﻿using Newtonsoft.Json;
using SPR.IdentityServer.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace SPR.IdentityServer.Api.DataContract.Users
{
    public class UserDto
    {
        /// <summary>
        /// User's id
        /// </summary>
        [JsonProperty(Required = Required.Default)]
        public Guid Id { get; set; }

        /// <summary>
        /// Disabled flag
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public bool IsDisabled { get; set; }

        /// <summary>
        /// User's name
        /// </summary>        
        [JsonProperty(Required = Required.AllowNull)]
        public string? Name { get; set; }

        /// <summary>
        /// User's login
        /// </summary>
        [MinLength(2)]
        [MaxLength(100)]
        [JsonProperty(Required = Required.Always)]
        public string Login { get; set; } = null!;

        /// <summary>
        /// Login type
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public LoginTypes LoginType { get; set; }
    }
}

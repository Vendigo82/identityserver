﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Moq;
using SPR.IdentityServer.Api.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Api.Services.PasswordServiceTests
{
    public class SetPasswordTests : PasswordServiceBaseTests
    {
        public SetPasswordTests()
        {
            context.Users.Add(new DAL.Model.User {
                Id = Guid.NewGuid(),
                Salt = "oldsalt",
                PasswordHash = "oldhash",
                LoginTypeId = (short)Enums.LoginTypes.Local
            });
            context.SaveChangesAsync();
        }

        [Theory, AutoData]
        public async Task SuccessTest(string password, string salt, string hash)
        {
            // setup
            var dbuser = context.Users.First();
            saltMock.Setup(f => f.GenSalt()).Returns(salt);
            hasherMock.Setup(f => f.HashPassword(salt, password)).Returns(hash);

            // action
            await service.SetPasswordAsync(dbuser.Id, password);

            // asserts
            saltMock.Verify(f => f.GenSalt(), Times.Once);
            hasherMock.Verify(f => f.HashPassword(salt, password), Times.Once);
            mockRepository.VerifyNoOtherCalls();

            dbuser.Should().BeEquivalentTo(new {
                Salt = salt,
                PasswordHash = hash
            });
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public async Task EmptyPassword(string password)
        {
            // setup
            var dbuser = context.Users.First();

            // action
            await service.SetPasswordAsync(dbuser.Id, password);

            // asserts
            mockRepository.VerifyNoOtherCalls();

            dbuser.Salt.Should().BeNull();
            dbuser.PasswordHash.Should().BeNull();
        }

        [Fact]
        public async Task UserNotFoundTest()
        {
            // setup

            // action
            Func<Task> action = () => service.SetPasswordAsync(Guid.NewGuid(), "psw");

            // asserts
            await action.Should().ThrowExactlyAsync<UserNotFoundException>();
        }

        [Fact]
        public async Task DomainUserTest()
        {
            // setup
            var dbuser = context.Users.First();
            dbuser.LoginTypeId = (short)Enums.LoginTypes.Domain;
            context.SaveChanges();

            // action
            Func<Task> action = () => service.SetPasswordAsync(dbuser.Id, "psw");

            // asserts
            await action.Should().ThrowExactlyAsync<ChangePasswordViolationException>();
        }
    }
}

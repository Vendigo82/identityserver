﻿using AutoMapper;
using SPR.IdentityServer.Api.DataContract.Clients;
using SPR.IdentityServer.Api.DataContract.Users;
using SPR.IdentityServer.DAL.Model;
using System;

namespace SPR.IdentityServer.Api.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Client, ClientDto>()
                .ReverseMap()
                .ForMember(d => d.Id, m => m.Ignore());

            CreateMap<User, UserDto>()
                .ForMember(d => d.LoginType, m => m.MapFrom(s => (Enums.LoginTypes)s.LoginTypeId))
                .ReverseMap()
                .ForMember(d => d.LoginTypeId, m => m.MapFrom(s => (short)s.LoginType))
                .ForMember(d => d.LoginType, m => m.Ignore());
        }
    }
}

﻿using IdentityServer4.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SPR.IdentityServer.Api.DataContract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Controllers.LocalApi
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [ProducesErrorResponseType(typeof(ProblemDetails))]
    [Consumes(MediaTypeNames.Application.Json)]
    public class TemplateController : ControllerBase
    {
        /// <summary>
        /// Make hash for client's secret
        /// </summary>
        /// <param name="secret"></param>
        /// <returns></returns>
        [HttpGet("hash/{secret}")]
        public HashResponse Hash([FromRoute] string secret)
        {
            var s = new Secret(secret.Sha256());
            return new HashResponse { Hash = s.Value };
        }
        
        [HttpGet("[action]")]
        public ListContainer<string> Configuration()
        {
            var path = Path.Combine("Template", "Configuration");
            var files = Directory.GetFiles(path, "*.json");
            return new ListContainer<string> { Items = files.Select(i => i[(path.Length+1)..^5]) };
        }

        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(FileContentResult))]
        [HttpGet("[action]/{name}")]
        public async Task<IActionResult> Configuration([FromRoute] string name)
        {
            var fileName = Path.Combine("Template", "Configuration", name + ".json");
            if (!System.IO.File.Exists(fileName))
                return NoContent();

            var content = await System.IO.File.ReadAllBytesAsync(fileName);
            return File(content, MediaTypeNames.Application.Json);
        }
    }

    public class HashResponse
    {
        [JsonProperty(Required = Required.Always)]
        public string Hash { get; set; }
    }    
}

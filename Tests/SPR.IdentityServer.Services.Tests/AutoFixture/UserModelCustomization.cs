﻿using AutoFixture;
using SPR.IdentityServer.DataModel;
using SPR.IdentityServer.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Services.AutoFixture
{
    /// <summary>
    /// Customize <see cref="UserModel"/> generation. Set <see cref="UserModel.LoginType"/> to <see cref="Enums.LoginTypes.Local"/>
    /// </summary>
    public class UserModelCustomization : ICustomization
    {
        private readonly Enums.LoginTypes loginType;

        public UserModelCustomization(LoginTypes loginType)
        {
            this.loginType = loginType;
        }

        public void Customize(IFixture fixture)
        {
            fixture.Customize<UserModel>(f => f.With(p => p.LoginType, loginType));
        }
    }
}

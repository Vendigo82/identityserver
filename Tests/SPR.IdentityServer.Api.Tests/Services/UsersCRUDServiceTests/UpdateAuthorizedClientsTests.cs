﻿using AutoFixture;
using FluentAssertions;
using SPR.IdentityServer.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Api.Services.UsersCRUDServiceTests
{
    public class UpdateAuthorizedClientsTests : UsersCRUDServiceBaseTests
    {
        public UpdateAuthorizedClientsTests()
        {
            context.Users.AddRange(fixture.Create<User>());
            context.Clients.AddRange(Enumerable.Range(1, 3).Select(i => fixture.Create<Client>()));
            context.SaveChanges();

            var user = context.Users.First();
            foreach (var c in context.Clients.Take(2))
                user.UserClients.Add(new UserClient { UserId = user.Id, ClientId = c.Id });

            context.SaveChanges();
        }

        [Fact]
        public async Task UpdateTest()
        {
            // setup
            var dbitem = context.Users.First();
            var revoke = new[] { dbitem.UserClients.First().ClientId };
            var grant = new[] { context.Clients.First(i => i.UserClients.Any() == false).Id };

            // action
            await service.UpdateAuthorizedClientsAsync(dbitem.Id, revoke, grant);

            // asserts
            context.ChangeTracker.HasChanges().Should().BeFalse();
            dbitem.UserClients.Should()
                .HaveCount(2).And
                .ContainSingle(i => i.ClientId == grant[0]).And
                .NotContain(i => i.ClientId == revoke[0]);
        }

        [Fact]
        public async Task AddAlreadyExistedTest()
        {
            // setup
            var dbitem = context.Users.First();
            var grant = new[] { dbitem.UserClients.First().ClientId };

            // action
            await service.UpdateAuthorizedClientsAsync(dbitem.Id, Enumerable.Empty<Guid>(), grant);

            // asserts
            context.ChangeTracker.HasChanges().Should().BeFalse();
            dbitem.UserClients.Should()
                .HaveCount(2).And
                .ContainSingle(i => i.ClientId == grant[0]);
        }

        [Fact]
        public async Task RemoveNotExistedTest()
        {
            // setup
            var dbitem = context.Users.First();
            var revoke = new[] { context.Clients.First(i => i.UserClients.Any() == false).Id };

            // action
            await service.UpdateAuthorizedClientsAsync(dbitem.Id, revoke, Enumerable.Empty<Guid>());

            // asserts
            context.ChangeTracker.HasChanges().Should().BeFalse();
            dbitem.UserClients.Should()
                .HaveCount(2);
        }
    }
}

﻿using AutoFixture;
using AutoFixture.Xunit2;
using FluentAssertions;
using SPR.IdentityServer.Api.DataContract.Users;
using SPR.IdentityServer.Api.Exceptions;
using SPR.IdentityServer.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Api.Services.UsersCRUDServiceTests
{
    public class UpdateTests : UsersCRUDServiceBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(UserDto user)
        {
            // setup
            context.Users.Add(fixture.Build<User>().Without(i => i.LoginType).Without(i => i.UserClients).With(i => i.Id, user.Id).Create());
            context.SaveChanges();

            // action
            await service.UpdateAsync(user);

            // asserts
            var dbitem = context.Users.Should().ContainSingle(i => i.Id == user.Id).Which;
            dbitem.Should().BeEquivalentTo(user, o => o.Excluding(i => i.LoginType));
            user.LoginType.Should().Be((Enums.LoginTypes)dbitem.LoginTypeId);
        }

        [Theory, AutoData]
        public async Task NotFoundTest(UserDto user)
        {
            // setup

            // action
            Func<Task> action = () => service.UpdateAsync(user);

            // asserts
            await action.Should().ThrowExactlyAsync<UserNotFoundException>();
            context.Users.Should().BeEmpty();
        }

        [Theory, AutoData]
        public async Task UpdateProfileTest(Guid userId, UserProfileUpdateDto user)
        {
            // setup            
            context.Users.Add(fixture.Build<User>().Without(i => i.LoginType).Without(i => i.UserClients).With(i => i.Id, userId).Create());
            context.SaveChanges();

            // action
            await service.UpdateAsync(userId, user);

            // asserts
            var dbitem = context.Users.Should().ContainSingle(i => i.Id == userId).Which;
            dbitem.Should().BeEquivalentTo(user);
        }

        [Theory, AutoData]
        public async Task UpdateProfileNotFoundTest(Guid userId, UserProfileUpdateDto user)
        {
            // setup

            // action
            Func<Task> action = () => service.UpdateAsync(userId, user);

            // asserts
            await action.Should().ThrowExactlyAsync<UserNotFoundException>();
            context.Users.Should().BeEmpty();
        }
    }
}

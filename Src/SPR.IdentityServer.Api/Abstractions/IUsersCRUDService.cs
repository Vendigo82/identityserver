﻿using SPR.IdentityServer.Api.DataContract.Clients;
using SPR.IdentityServer.Api.DataContract.Users;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Api.Abstractions
{
    public interface IUsersCRUDService
    {
        /// <summary>
        /// Load list or users
        /// </summary>
        /// <param name="offset">returned record's offset</param>
        /// <param name="count">returned record's count</param>
        /// <param name="filter">filter's value. Filteting by login or name</param>
        /// <returns>list of users</returns>
        public Task<IEnumerable<UserDto>> GetListAsync(int offset, int count, string? filter);

        /// <summary>
        /// Get list of users by id
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public Task<IEnumerable<UserDto>> GetListAsync(IEnumerable<Guid> ids);

        /// <summary>
        /// Get single user by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>User or null if not found</returns>
        public Task<UserDto?> GetAsync(Guid id);

        /// <summary>
        /// Get count of users
        /// </summary>
        /// <returns></returns>
        public Task<int> GetCountAsync(string? filter);

        /// <summary>
        /// Insert new user
        /// </summary>
        /// <param name="user"></param>
        /// <returns>Inserted user's id</returns>
        /// <exception cref="Exceptions.ConstraintViolationException">Contraint violation on insert up update user</exception>
        public Task<(Guid Id, bool Inserted)> InsertAsync(UserDto user);

        /// <summary>
        /// Update existed user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        /// <exception cref="Exceptions.UserNotFoundException">user not found</exception>
        /// <exception cref="Exceptions.ConstraintViolationException">Contraint violation on insert up update user</exception>
        public Task UpdateAsync(UserDto user);

        /// <summary>
        /// Update user's profile
        /// </summary>
        /// <param name="userId">user's id</param>
        /// <param name="user"></param>
        /// <returns></returns>
        /// <exception cref="Exceptions.UserNotFoundException">user not found</exception>
        public Task UpdateAsync(Guid userId, UserProfileUpdateDto user);

        /// <summary>
        /// Get list of authorized clients for user
        /// </summary>
        /// <param name="userId">User's id</param>
        /// <returns></returns>
        public Task<IEnumerable<ClientDto>> GetAuthorizedClientsAsync(Guid userId);

        /// <summary>
        /// Update list of authorized clients
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="revokeClients"></param>
        /// <param name="grantClients"></param>
        /// <returns>true if updated. false if user does not found</returns>
        public Task UpdateAuthorizedClientsAsync(Guid userId, IEnumerable<Guid> revokeClients, IEnumerable<Guid> grantClients);
    }
}

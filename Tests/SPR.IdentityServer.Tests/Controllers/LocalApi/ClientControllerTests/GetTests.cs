﻿using AutoFixture.Xunit2;
using FluentAssertions;
using FluentAssertions.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc;
using Moq;
using SPR.IdentityServer.Api.DataContract;
using SPR.IdentityServer.Api.DataContract.Clients;
using SPR.IdentityServer.Api.DataContract.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Controllers.LocalApi.ClientControllerTests
{
    public class GetTests : ClientControllerBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(ClientDto client)
        {
            // setup
            clientsMock.Setup(f => f.GetAsync(client.Id)).ReturnsAsync(client);

            // action
            var result = await controller.GetClientAsync(client.Id);

            // asserts
            result.Should().BeOkObjectResult().WithValueEquivalentTo(client);
            clientsMock.Verify(f => f.GetAsync(client.Id), Times.Once);
        }

        [Theory, AutoData]
        public async Task NotFoundTest(Guid clientId)
        {
            // setup
            clientsMock.Setup(f => f.GetAsync(clientId)).ReturnsAsync((ClientDto)null);

            // action
            var result = await controller.GetClientAsync(clientId);

            // asserts
            result.Should().BeNotFoundObjectResult().Value.Should().BeOfType<ProblemDetails>();
            clientsMock.Verify(f => f.GetAsync(clientId), Times.Once);
        }
    }
}

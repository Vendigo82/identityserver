﻿using Microsoft.Extensions.DependencyInjection;
using SPR.IdentityServer.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.IntegrationTests
{
    public class BaseTests : IClassFixture<WebServiceTestFixture>, IDisposable
    {
        protected readonly WebServiceTestFixture fixture;

        protected readonly IServiceScope _scope;

        /// <summary>
        /// Database entities test model with proxy-classes
        /// </summary>
        public UsersContext Context { get; }

        protected readonly List<Guid> createdUsers = new();

        public BaseTests(WebServiceTestFixture fixture)
        {
            this.fixture = fixture;            

            _scope = this.fixture.ServiceProvider.CreateScope();
            Context = _scope.ServiceProvider.GetRequiredService<UsersContext>();
        }

        public virtual void Dispose()
        {
            foreach (var u in createdUsers) {
                var dbitem = Context.Users.FirstOrDefault(i => i.Id == u);
                if (dbitem != null)
                    Context.Users.Remove(dbitem);
                Context.SaveChanges();
            }

            _scope.Dispose();
        }
    }
}

﻿using AutoFixture.Xunit2;
using FluentAssertions;
using FluentAssertions.AspNetCore.Mvc;
using Moq;
using SPR.IdentityServer.Api.DataContract;
using SPR.IdentityServer.Api.DataContract.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Controllers.LocalApi.UsersControllerTests
{
    public class GetListTests : UsersControllerBaseTests
    {
        [Theory]
        [InlineAutoData(null, null, null)]
        [InlineAutoData(10, 20)]
        public async Task GetListTest(int? offset, int? count, string filter, IEnumerable<UserDto> usersList)
        {
            // setup
            serviceMock.Setup(f => f.GetListAsync(offset ?? 0, count ?? settingsMock.Object.Value.DefaultCount, filter))
                .ReturnsAsync(usersList);

            // action
            var result = await controller.GetListAsync(offset, count, filter);

            // asserts
            var response = result.Should()
                .BeOkObjectResult().Value.Should()
                .BeOfType<PartialListContainer<UserDto>>().Which;
            response.Items.Should().BeSameAs(usersList);
            response.MaxCount.Should().Be(settingsMock.Object.Value.MaxCount);
            response.RequestedCount.Should().Be(count ?? settingsMock.Object.Value.DefaultCount);

            serviceMock.Verify(f => f.GetListAsync(offset ?? 0, count ?? settingsMock.Object.Value.DefaultCount, filter), Times.Once);
        }

        [Fact]
        public async Task MaximumValueTest()
        {
            // setup
            var requestedCount = settingsMock.Object.Value.MaxCount * 2;

            // action
            var result = await controller.GetListAsync(null, requestedCount, null);

            // asserts
            var response = result.Should()
                .BeOkObjectResult().Value.Should()
                .BeOfType<PartialListContainer<UserDto>>().Which;

            response.MaxCount.Should().Be(settingsMock.Object.Value.MaxCount);
            response.RequestedCount.Should().Be(settingsMock.Object.Value.MaxCount);

            serviceMock.Verify(f => f.GetListAsync(0, settingsMock.Object.Value.MaxCount, null), Times.Once);
        }
    }
}

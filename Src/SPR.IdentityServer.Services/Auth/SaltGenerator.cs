﻿using SPR.IdentityServer.Abstractions.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Services.Auth
{
    public class SaltGenerator : ISaltGenerator
    {
        private const int SaltLength = 64;

        public string GenSalt()
        {
            return GetSalt(SaltLength);

            static string GetSalt(int size)
            {
                var saltBytes = new byte[size];
                using var provider = new RNGCryptoServiceProvider();
                provider.GetNonZeroBytes(saltBytes);
                var salt = Convert.ToBase64String(saltBytes);

                return salt;
            }
        }

        
    }
}

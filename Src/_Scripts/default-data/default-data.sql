insert into identityserver.client (system_name, title, require_permission, configuration) values
 ('is_admin', 'Identity server administrative site', true, '{"ClientSecrets":[{"Value":"4yUiRj9Q1giqQyeLKCc0TO6Gn9kaj8z6vLSIL3ObXFo="}],"AllowedGrantTypes":["authorization_code"],"RequireConsent":false,"RequirePkce":true,"RedirectUris":["http://localhost:8001/signin-oidc"],"PostLogoutRedirectUris":["http://localhost:8001/signout-callback-oidc"],"AllowedScopes":["openid","profile","users_api.read","users_api.write_clients","users_api.write_users","users_api.change_password"],"AllowOfflineAccess":true,"UpdateAccessTokenClaimsOnRefresh":true}')
,('profile', 'Профиль пользователя', false, '{"ClientSecrets":[{"Value":"oJ7zXVYxDBG1UM58kQ1TUahzPT4avMT02sNmsgnYbGI="}],"AllowedGrantTypes":["authorization_code"],"RequireConsent":false,"RequirePkce":true,"RedirectUris":["http://localhost:8002/signin-oidc"],"PostLogoutRedirectUris":["http://localhost:8002/signout-callback-oidc"],"AllowedScopes":["openid","profile","users_api.profile"],"AllowOfflineAccess":true,"UpdateAccessTokenClaimsOnRefresh":true}')
--,('postman', 'postman', false, '{"AllowedGrantTypes":["client_credentials","password"],"ClientSecrets":[{"Value":"K7gNU3sdo+OL0wNhqoVWhr3g6s1xYv72ol/pe/Unols="}],"AllowedScopes":["openid","profile","users_api.read","users_api.write_clients","users_api.write_users","users_api.change_password"],"AccessTokenLifetime":600000}')
on conflict do nothing;

insert into identityserver.user (login, login_type_id) values
('sa', (select id from identityserver.login_type where system_name = 'local'))
on conflict do nothing;
		
insert into identityserver.user_client (user_id, client_id) values
((select id from identityserver.user where login='sa'), (select id from identityserver.client where system_name='is_admin'))
on conflict do nothing;
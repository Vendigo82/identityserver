﻿using AutoFixture;
using AutoFixture.Xunit2;
using FluentAssertions;
using SPR.IdentityServer.Api.DataContract.Clients;
using SPR.IdentityServer.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Api.Services.ClientsCRUDServiceTests
{
    public class UpdateTests : ClientsCRUDServiceBaseTests
    {
        [Theory, AutoData]
        public async Task UpdateSuccessTest(ClientDto client)
        {
            // setup
            context.Clients.Add(fixture.Build<Client>().Without(i => i.UserClients).With(i => i.Id, client.Id).Create());
            await context.SaveChangesAsync();

            // action
            await service.UpdateAsync(client);

            // asserts
            context.Clients.Should()
                .ContainSingle(i => i.Id == client.Id).Which.Should()
                .BeEquivalentTo(client);            
        }

        [Theory, AutoData]
        public async Task UpdateNotFoundTests(ClientDto client)
        {
            // setup

            // action
            Func<Task> action = () => service.UpdateAsync(client);

            // asserts
            await action.Should().ThrowExactlyAsync<Exceptions.ClientNotFoundException>();
            context.Clients.Should().NotContain(i => i.Id == client.Id);
        }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Api.DataContract
{
    public class ObjectIdDto
    {
        [JsonProperty(Required = Required.Always)]
        public Guid Id { get; init; }
    }
}

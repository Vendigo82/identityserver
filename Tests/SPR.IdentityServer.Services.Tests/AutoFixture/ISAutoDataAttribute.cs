﻿using AutoFixture;
using AutoFixture.Xunit2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit.Sdk;

namespace SPR.IdentityServer.Services.AutoFixture
{
    public class ISAutoDataAttribute : AutoDataAttribute
    {
        public ISAutoDataAttribute(Enums.LoginTypes loginType = Enums.LoginTypes.Local) 
            : base(() => Create(loginType))
        {
        }

        public static Fixture Create(Enums.LoginTypes loginType)
        {
            var fixture = new Fixture();
            fixture.Customize(new UserModelCustomization(loginType));
            return fixture;
        }
    }

    public class InlineISAutoDataAttribute : InlineAutoDataAttribute
    {
        public InlineISAutoDataAttribute(Enums.LoginTypes loginType, params object[] values) 
            : base(new ISAutoDataAttribute(loginType), values)
        {
        }
    }
}

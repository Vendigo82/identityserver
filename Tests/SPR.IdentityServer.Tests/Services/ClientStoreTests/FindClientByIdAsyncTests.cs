﻿using AutoFixture.Xunit2;
using FluentAssertions;
using SPR.IdentityServer.DAL.Model;
using SPR.IdentityServer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Tests.Services.ClientStoreTests
{
    public class FindClientByIdAsyncTests
    {
        private readonly UsersContext context = Tools.InitMemoryDd();
        private readonly ClientStore target;

        public FindClientByIdAsyncTests()
        {
            target = new(context);
        }

        [Theory]
        [InlineAutoData(true)]
        [InlineAutoData(false)]
        public async Task ClientHasConfigurationAndShouldBeFoundTest(bool disabled, string clientId, string redirectPath)
        {
            // setup
            context.Clients.Add(new Client {
                Configuration = $"{{ \"RedirectUris\": [ \"{redirectPath}\" ] }}",
                SystemName = clientId,
                IsDisabled = disabled
            });
            await context.SaveChangesAsync();

            // action
            var result = await target.FindClientByIdAsync(clientId);

            // asserts
            var expectedClient = new IdentityServer4.Models.Client {
                ClientId = clientId,
                Enabled = !disabled,
                RedirectUris = new HashSet<string> { redirectPath }
            };

            result.Should().NotBeNull().And.BeEquivalentTo(expectedClient);            
        }

        [Theory, AutoData]
        public async Task ClientHasNullConfigurationAndShouldBeFoundTest(string clientId)
        {
            // setup
            context.Clients.Add(new Client {
                Configuration = null,
                SystemName = clientId
            });
            await context.SaveChangesAsync();

            // action
            var result = await target.FindClientByIdAsync(clientId);

            // asserts
            var expectedClient = new IdentityServer4.Models.Client {
                ClientId = clientId
            };

            result.Should().NotBeNull().And.BeEquivalentTo(expectedClient);
        }

        [Theory, AutoData]
        public async Task ClientNotFoundTest(string clientId)
        {
            // setup

            // action
            var result = await target.FindClientByIdAsync(clientId);

            // asserts
            result.Should().BeNull();
        }
    }
}

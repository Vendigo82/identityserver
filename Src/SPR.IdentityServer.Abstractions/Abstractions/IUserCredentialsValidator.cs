﻿using SPR.IdentityServer.DataModel;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Abstractions
{
    public enum ValidateResult
    {
        Success,

        InvalidLoginOrPassword,

        ClientNotAllowed
    }

    public interface IUserCredentialsValidator
    {
        ///// <summary>
        ///// Search user by Id
        ///// </summary>
        ///// <param name="id">user Id</param>
        ///// <returns>User or null</returns>
        //Task<UserModel?> SearchAsync(Guid id);

        /// <summary>
        /// Validate user login and password. 
        /// If <paramref name="client"/> is not null then also verify can user login to client and project or not.
        /// </summary>
        /// <param name="login">User login</param>
        /// <param name="password">User password</param>
        /// <param name="client">Client (application) to login. If null, then login to identity server itself</param>
        /// <param name="projectId">Project id</param>
        /// <returns>Result and user info</returns>
        Task<(ValidateResult Result, UserModel? User)> ValidateCredentialsAsync(string login, string? password, string? client);

        ///// <summary>
        ///// Can user login to specific client or not
        ///// </summary>
        ///// <param name="userId">User's id</param>
        ///// <param name="client">Client which client want to login</param>
        ///// <returns>true if can login</returns>
        //Task<bool> CanLogin(Guid userId, string? client);
    }

}

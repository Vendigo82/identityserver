﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Enums
{
    public enum LoginTypes
    {
        /// <summary>
        /// Local Identity server user
        /// </summary>
        Local = 1,

        /// <summary>
        /// Domain user
        /// </summary>
        Domain = 2,
    }
}

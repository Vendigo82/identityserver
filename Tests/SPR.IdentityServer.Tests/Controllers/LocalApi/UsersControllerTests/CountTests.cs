﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Controllers.LocalApi.UsersControllerTests
{
    public class CountTests : UsersControllerBaseTests
    {
        [Theory, AutoData]
        public async Task CountTest(string filter, int count)
        {
            // setup
            serviceMock.Setup(f => f.GetCountAsync(filter)).ReturnsAsync(count);

            // action
            var result = await controller.GetCountAsync(filter);

            // setup
            result.Count.Should().Be(count);
            serviceMock.Verify(f => f.GetCountAsync(filter), Times.Once);
        }
    }
}

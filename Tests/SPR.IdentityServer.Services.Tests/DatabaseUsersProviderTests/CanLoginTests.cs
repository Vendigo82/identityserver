﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Services.DatabaseUsersProviderTests
{
    public class CanLoginTests : DatabaseUsersProviderBaseTests
    {
        private readonly Guid userId;

        public CanLoginTests()
        {
            InitDb();
            userId = context.Users.First().Id;
        }

        private void InitDb()
        {
            var user = context.Users.First();
            user.UserClients = new List<DAL.Model.UserClient>();
            
            context.Clients.Add(new DAL.Model.Client {
                Id = Guid.NewGuid(),
                IsDisabled = false,
                SystemName = "allowed",
                RequirePermission = true
            });

            context.Clients.Add(new DAL.Model.Client {
                Id = Guid.NewGuid(),
                IsDisabled = true,
                SystemName = "disabled",
                RequirePermission = true
            });

            context.Clients.Add(new DAL.Model.Client {
                Id = Guid.NewGuid(),
                IsDisabled = false,
                SystemName = "notAllowed",
                RequirePermission = true
            });

            context.Clients.Add(new DAL.Model.Client {
                Id = Guid.NewGuid(),
                IsDisabled = false,
                SystemName = "public",
                RequirePermission = false
            });

            context.SaveChanges();

            user.UserClients.Add(new DAL.Model.UserClient { UserId = user.Id, ClientId = context.Clients.First(i => i.SystemName == "allowed").Id });
            user.UserClients.Add(new DAL.Model.UserClient { UserId = user.Id, ClientId = context.Clients.First(i => i.SystemName == "disabled").Id });
            context.SaveChanges();
        }

        [Fact]
        public async Task AllowedClientTest()
        {
            // setup

            // action
            var result = await store.CanLoginAsync(userId, "allowed");

            // asserts
            result.Should().BeTrue();
        }

        [Fact]
        public async Task DisabledClientTest()
        {
            // setup

            // action
            var result = await store.CanLoginAsync(userId, "disabled");

            // asserts
            result.Should().BeFalse();
        }

        [Fact]
        public async Task NotAllowedClientTest()
        {
            // setup

            // action
            var result = await store.CanLoginAsync(userId, "notAllowed");

            // asserts
            result.Should().BeFalse();
        }

        [Theory]
        [InlineData("allowed")]
        [InlineData("public")]
        public async Task DisabledUserTest(string clientName)
        {
            // setup
            var dbuser = context.Users.First(i => i.Id == userId);
            dbuser.IsDisabled = true;
            context.SaveChanges();

            // action
            var result = await store.CanLoginAsync(userId, clientName);

            // asserts
            result.Should().BeFalse();
        }

        [Fact]
        public async Task ClientWithoutRequirePermission()
        {
            // setup            

            // action
            var result = await store.CanLoginAsync(userId, "allowed");

            // asserts
            result.Should().BeTrue();
        }
    }
}

﻿using IdentityModel;
using IdentityServer4;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.IdentityServer
{
    public static class AuthPolicy
    {
        public const string ReadPolicy = "LocalApiReadPolicy";
        public const string WriteClientsPolicy = "LocalApiWriteClientsPolicy";
        public const string WriteUsersPolicy = "LocalApiWriteUsersPolicy";
        public const string ChangePasswordPolicy = "ChangePasswordPolicy";
        public const string ProfilePolicy = "ProfilePolicy";
    }

    public static class AuthorizationOptionsExtensions
    {
        private const string ReadScope = "users_api.read";
        private const string WriteClientsScope = "users_api.write_clients";
        private const string WriteUsersScope = "users_api.write_users";
        private const string ChangePasswordScope = "users_api.change_password";
        private const string ProfileScope = "users_api.profile";

        /// <summary>
        /// Add authorization policies for local api
        /// </summary>
        /// <param name="options"></param>
        /// <param name="schemeName"></param>
        public static void ConfigureLocalApiPolicies(
            this AuthorizationOptions options,
            string schemeName = IdentityServerConstants.LocalApi.AuthenticationScheme)
        {
            options.AddPolicy(AuthPolicy.ReadPolicy, policy => {
                policy.AddAuthenticationSchemes(schemeName);
                policy.RequireClaim(JwtClaimTypes.Scope, ReadScope);
            });
            options.AddPolicy(AuthPolicy.WriteClientsPolicy, policy => {
                policy.AddAuthenticationSchemes(schemeName);
                policy.RequireClaim(JwtClaimTypes.Scope, WriteClientsScope);
            });
            options.AddPolicy(AuthPolicy.WriteUsersPolicy, policy => {
                policy.AddAuthenticationSchemes(schemeName);
                policy.RequireClaim(JwtClaimTypes.Scope, WriteUsersScope);
            });
            options.AddPolicy(AuthPolicy.ChangePasswordPolicy, policy => {
                policy.AddAuthenticationSchemes(schemeName);
                policy.RequireClaim(JwtClaimTypes.Scope, ChangePasswordScope);
            });
            options.AddPolicy(AuthPolicy.ProfilePolicy, policy => {
                policy.AddAuthenticationSchemes(schemeName);
                policy.RequireClaim(JwtClaimTypes.Subject);
                policy.RequireClaim(JwtClaimTypes.Scope, ProfileScope);
            });
        }
    }
}

﻿using AutoFixture.Xunit2;
using FluentAssertions;
using FluentAssertions.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc;
using Moq;
using SPR.IdentityServer.Api.Abstractions;
using SPR.IdentityServer.Api.DataContract.Users;
using SPR.IdentityServer.Api.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Tests.Controllers.LocalApi.ProfileControllerTests
{
    public class ChangePasswordAsyncTests : ProfileControllerBaseTests
    {
        readonly Mock<IPasswordService> serviceMock = new();

        [Theory, AutoData]
        public async Task SuccessTest(ProfileChangePasswordRequest request)
        {
            // setup

            // action
            var result = await controller.ChangePasswordAsync(request, serviceMock.Object);

            // asserts
            result.Should().BeOkResult();
            serviceMock.Verify(f => f.SetPasswordAsync(userId, request.Password, request.OldPassword), Times.Once);            
        }

        [Theory, AutoData]
        public async Task InvalidOldPassword(ProfileChangePasswordRequest request)
        {
            // setup
            serviceMock.Setup(f => f.SetPasswordAsync(userId, request.Password, request.OldPassword))
                .ThrowsAsync(new InvalidOldPasswordException());

            // action
            var result = await controller.ChangePasswordAsync(request, serviceMock.Object);

            // asserts
            result.Should()
                .BeOfType<BadRequestObjectResult>().Which.Value.Should()
                .BeOfType<ValidationProblemDetails>().Which.Errors.Should()
                .BeEquivalentTo(new Dictionary<string, string[]>() { { "oldPassword", new[] { "Invalid old password" } } });
        }
    }
}

﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SPR.IdentityServer.Api.Abstractions;
using SPR.IdentityServer.Api.DataContract;
using SPR.IdentityServer.Api.DataContract.Clients;
using SPR.IdentityServer.Api.DataContract.Users;
using SPR.IdentityServer.Filters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Controllers.LocalApi
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [ProducesErrorResponseType(typeof(ProblemDetails))]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiExceptionFilter]
    public class UsersController : ControllerBase
    {
        private readonly IUsersCRUDService _service;
        private readonly IOptions<Settings> _settings;
        private readonly ILogger<UsersController> _logger;

        public const string GetUserByIdActionName = "GetUser";

        public UsersController(IUsersCRUDService service, IOptions<Settings> settings, ILogger<UsersController> logger)
        {
            _service = service ?? throw new ArgumentNullException(nameof(service));
            _settings = settings ?? throw new ArgumentNullException(nameof(settings));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        /// <summary>
        /// Request list of users
        /// </summary>
        /// <param name="count">count of users to retrieve</param>
        /// <param name="offset">offset in list to retrieve</param>
        /// <param name="filter">filter value. Users will filter by name and login</param>
        /// <returns></returns>
        [HttpGet()]
        [Authorize(AuthPolicy.ReadPolicy)]
        [ProducesResponseType(typeof(PartialListContainer<UserDto>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetListAsync([FromQuery] int? offset, [FromQuery] int? count, [FromQuery] string filter)
        {
            _logger.LogDebug("Request list of users with filter '{filter}' from {offset}, count {count}", filter, offset, count);
            count = GetCount();
            var list = await _service.GetListAsync(offset ?? 0, count.Value, filter);
            return Ok(new PartialListContainer<UserDto> {
                Items = list,
                MaxCount = _settings.Value.MaxCount,
                RequestedCount = count.Value
            });

            int GetCount() => Math.Min(count ?? _settings.Value.DefaultCount, _settings.Value.MaxCount);
        }

        /// <summary>
        /// Returns list of users by id
        /// </summary>
        /// <param name="id">user's ids</param>
        /// <returns></returns>
        [HttpGet("bunch")]
        [Authorize(AuthPolicy.ReadPolicy)]
        [ProducesResponseType(typeof(ListContainer<UserDto>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetBunchAsync([FromQuery] IEnumerable<Guid> id)
        {
            if (id.Any())
            {
                var list = await _service.GetListAsync(id);
                return Ok(new ListContainer<UserDto> {
                    Items = list
                });
            }
            else
                return Ok(new ListContainer<UserDto> { 
                    Items = Enumerable.Empty<UserDto>()
                });
        }

        /// <summary>
        /// Get user by id
        /// </summary>
        /// <param name="id">User's id</param>
        /// <returns>User info</returns>
        /// <response code="200">user was found</response>
        /// <response code="404">user does not found</response>
        [HttpGet("{id}", Name = GetUserByIdActionName)]
        [Authorize(AuthPolicy.ReadPolicy)]
        [ProducesResponseType(typeof(UserDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetUserAsync([FromRoute] Guid id)
        {
            _logger.LogDebug("Request single user with id {UserId}", id);
            var user = await _service.GetAsync(id);
            if (user == null) {
                _logger.LogDebug("User with id {UserId} does not found", id);
                return NotFound(new ProblemDetails { Title = "User not found" });
            }

            _logger.LogDebug("User with id {UserId}, login '{Login}' is {@data}", id, user.Login, user);
            return Ok(user);
        }

        /// <summary>
        /// Get total count of users
        /// </summary>
        /// <param name="filter">filter value</param>
        /// <returns></returns>
        [HttpGet("count")]
        [ProducesResponseType(typeof(CountDto), StatusCodes.Status200OK)]
        public async Task<CountDto> GetCountAsync([FromQuery] string filter)
        {
            var count = await _service.GetCountAsync(filter);
            _logger.LogDebug("User's count with filter '{filter}': {count}", filter, count);

            return new CountDto { Count = count };
        }

        /// <summary>
        /// Insert new user
        /// </summary>
        /// <param name="user">user's model</param>
        /// <returns></returns>
        /// <response code="201">Successfully created</response>
        /// <response code="404">User does not found</response>
        /// <response code="409">There are another user with this login</response>
        [HttpPut]
        [Authorize(AuthPolicy.WriteUsersPolicy)]
        [ProducesResponseType(typeof(ObjectIdDto), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<IActionResult> InsertUserAsync([FromBody] UserDto user)
        {
            _logger.LogDebug("Inserting new user '{Login}': {@data}", user.Login, user);
            var (id, inserted) = await _service.InsertAsync(user);
            if (inserted)
                _logger.LogInformation("New user was inserted with id={UserId},  login '{Login}': {@data}", id, user.Login, user);
            else
                _logger.LogDebug("User with id={UserId},  login '{Login}' already exists: {@data}", id, user.Login, user);
            return CreatedAtAction(GetUserByIdActionName, new { id }, new ObjectIdDto { Id = id });
        }

        /// <summary>
        /// Update existed user
        /// </summary>
        /// <param name="user">user's model</param>
        /// <returns></returns>
        /// <response code="200">successfully updated</response>
        /// <response code="400">Invalid request</response>
        /// <response code="404">user does not found</response>
        /// <response code="409">There are another user with this login</response>
        [HttpPost()]
        [Authorize(AuthPolicy.WriteUsersPolicy)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<IActionResult> UpdateUserAsync([FromBody] UserDto user)
        {
            _logger.LogDebug("Updating user with id={UserId},  login '{Login}': {@data}", user.Id, user.Login, user);
            if (user.Id == Guid.Empty)
                return BadRequest(new ProblemDetails { Title = "Users's id is required" });            

            await _service.UpdateAsync(user);
            _logger.LogInformation("User with id={UserId} login '{Login}' was updated: {@data}", user.Id, user.Login, user);
            return Ok();
        }

        /// <summary>
        /// Get user's authorized clients
        /// </summary>
        /// <param name="id">User's id</param>
        /// <returns></returns>
        [HttpGet("{id}/clients")]
        [Authorize(AuthPolicy.ReadPolicy)]
        [ProducesResponseType(typeof(ListContainer<ClientDto>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetUserClientsAsync([FromRoute] Guid id)
        {
            _logger.LogDebug("Request auhtorized clients for user {UserId}", id);
            var clients = await _service.GetAuthorizedClientsAsync(id);
            _logger.LogDebug("Authorized clients for user {UserId} is {@data}", id, clients);
            var response = new ListContainer<ClientDto> { Items = clients };
            return Ok(response);
        }

        /// <summary>
        /// Update user's authorized clients
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("{id}/clients")]
        [Authorize(AuthPolicy.WriteUsersPolicy)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> UpdateUserClientsAsync([FromRoute] Guid id, [FromBody] UpdateUserClientsRequest model)
        {
            _logger.LogDebug("Update authorized clients for user {UserId}: {@data}", id, model);
            await _service.UpdateAuthorizedClientsAsync(id, model.Revoke ?? Enumerable.Empty<Guid>(), model.Grant ?? Enumerable.Empty<Guid>());
            _logger.LogInformation("Authorized clients for user {UserId} was updated: {@data}", id, model);
            return Ok();
        }

        /// <summary>
        /// Change password for the user
        /// </summary>
        /// <param name="id">User's id</param>
        /// <param name="model"></param>
        /// <param name="passwordService"></param>
        /// <returns></returns>
        /// <response code="404">user does not found</response>
        /// <response code="405">Can't change password for that user</response>
        [HttpPost("{id}/password")]
        [Authorize(AuthPolicy.ChangePasswordPolicy)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status405MethodNotAllowed)]
        public async Task<IActionResult> ChangePassword([FromRoute] Guid id, [FromBody] ChangePasswordRequest model, [FromServices] IPasswordService passwordService)
        {
            _logger.LogDebug("Changing password for user {UserId}", id);
            await passwordService.SetPasswordAsync(id, model.Password);
            _logger.LogInformation("Password for user {UserId} was changed", id);
            return Ok();
        }

        public class Settings
        {
            public int MaxCount { get; set; } = 100;

            public int DefaultCount { get; set; } = 10;
        }
    }
}

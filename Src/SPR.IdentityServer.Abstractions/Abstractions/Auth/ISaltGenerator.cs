﻿
namespace SPR.IdentityServer.Abstractions.Auth
{
    /// <summary>
    /// Provide salt generation
    /// </summary>
    public interface ISaltGenerator
    {
        /// <summary>
        /// Generate salt for password
        /// </summary>
        /// <returns></returns>
        public string GenSalt();
    }
}

﻿
using System.Threading.Tasks;

namespace SPR.IdentityServer.Abstractions.Auth
{
    public interface IPasswordValidator
    {
        bool Validate(string? salt, string? passwordHash, string? password);
    }
}

﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Services.Mapping.Tests
{
    public class EFMappingProfileTests
    {
        [Fact]
        public void ValidateConfigurationTest()
        {
            var cfg = new MapperConfiguration(c => c.AddProfile<EFMappingProfile>());
            cfg.AssertConfigurationIsValid();
        }
    }
}

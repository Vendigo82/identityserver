﻿using AutoFixture;
using FluentAssertions;
using SPR.IdentityServer.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Api.Services.UsersCRUDServiceTests
{
    public class GetTests : UsersCRUDServiceBaseTests
    {
        public GetTests()
        {
            context.Users.AddRange(Enumerable.Range(1, 2).Select(i => fixture.Create<User>()));
            context.SaveChanges();
        }

        [Fact]
        public async Task SuccessTest()
        {
            // setup
            var dbitem = context.Users.First();

            // action
            var result = await service.GetAsync(dbitem.Id);

            // asserts
            result.Should().NotBeNull();
            dbitem.Should().BeEquivalentTo(result, o => o.Excluding(i => i.LoginType));
            result.LoginType.Should().Be((Enums.LoginTypes)dbitem.LoginTypeId);
        }

        [Fact]
        public async Task NotFoundTest()
        {
            // setup

            // action
            var result = await service.GetAsync(Guid.NewGuid());

            // asserts
            result.Should().BeNull();
        }
    }
}

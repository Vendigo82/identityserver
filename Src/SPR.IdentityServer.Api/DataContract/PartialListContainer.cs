﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Api.DataContract
{
    public class PartialListContainer<T> : ListContainer<T>
    {
        /// <summary>
        /// Maximum count per request
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public int MaxCount { get; set; }

        /// <summary>
        /// Requested count
        /// </summary>
        [JsonProperty(Required = Required.Always)]
        public int RequestedCount { get; set; }
    }
}

insert into identityserver.client (system_name, title, require_permission, configuration) values
 ('test_client', 'Test client', false, '{"AllowedGrantTypes":["client_credentials"],"ClientSecrets":[{"Value":"K7gNU3sdo+OL0wNhqoVWhr3g6s1xYv72ol/pe/Unols="}],"AllowedScopes":["users_api.read"]}')
,('test_password_client', 'Test client password', false, '{"AllowedGrantTypes":["password"],"ClientSecrets":[{"Value":"K7gNU3sdo+OL0wNhqoVWhr3g6s1xYv72ol/pe/Unols="}],"AllowedScopes":["openid","profile","users_api.read"],"AccessTokenLifetime":600000}')
;

insert into identityserver.user (login, login_type_id) values
('sa', (select id from identityserver.login_type where system_name = 'local'))
on conflict do nothing;

insert into identityserver.user_client (user_id, client_id) values
((select id from identityserver.user where login='sa'), (select id from identityserver.client where system_name='test_password_client'))
on conflict do nothing;
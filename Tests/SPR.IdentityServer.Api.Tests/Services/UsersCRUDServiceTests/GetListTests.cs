﻿using AutoFixture;
using FluentAssertions;
using SPR.IdentityServer.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Api.Services.UsersCRUDServiceTests
{
    public class GetListTests : UsersCRUDServiceBaseTests
    {
        public GetListTests()
        {
            context.Users.AddRange(Enumerable.Range(1, 10).Select(i => fixture.Create<User>()));
            context.SaveChanges();
        }

        [Theory]
        [InlineData(0, 10)]
        [InlineData(5, 3)]
        [InlineData(0, 3)]
        [InlineData(8, 5)]
        public async Task GetListTest(int offset, int count)
        {
            // setup

            // action
            var result = await service.GetListAsync(offset, count, null);

            // asserts
            var expected = context.Users.OrderBy(i => i.Login).Skip(offset).Take(count).ToArray();
            result.Should().Equal(expected, (i, j) => i.Id == j.Id);
        }

        [Fact]
        public async Task FilterByLoginTest()
        {
            // setup
            var dbitem = context.Users.First();
            var filter = dbitem.Login[5..10];

            // action
            var result = await service.GetListAsync(0, 10, filter);

            // asserts
            result.Should().ContainSingle(i => i.Id == dbitem.Id);
        }

        [Fact]
        public async Task FilterByNameTest()
        {
            // setup
            var dbitem = context.Users.First();
            var filter = dbitem.Name[5..10];

            // action
            var result = await service.GetListAsync(0, 10, filter);

            // asserts
            result.Should().ContainSingle(i => i.Id == dbitem.Id);
        }

        [Fact]
        public async Task MappingTest()
        {
            // setup
            var dbitem = context.Users.First();

            // action
            var result = await service.GetListAsync(0, 10, dbitem.Login);

            // asserts
            var item = result.Should().ContainSingle(i => i.Id == dbitem.Id).Which;
            dbitem.Should().BeEquivalentTo(item, o => o.Excluding(i => i.LoginType));
            item.LoginType.Should().Be((Enums.LoginTypes)dbitem.LoginTypeId);
        }
    }
}

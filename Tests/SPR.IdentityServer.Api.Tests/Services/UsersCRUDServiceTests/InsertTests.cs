﻿using AutoFixture;
using AutoFixture.Xunit2;
using FluentAssertions;
using SPR.IdentityServer.Api.DataContract.Users;
using SPR.IdentityServer.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Api.Services.UsersCRUDServiceTests
{
    public class InsertTests : UsersCRUDServiceBaseTests
    {
        [Theory]
        [InlineAutoData(true)]
        [InlineAutoData(false)]
        public async Task SuccessTest(bool emptyId, UserDto user)
        {
            // setup
            if (emptyId)
                user.Id = Guid.Empty;

            // action
            var (id, inserted) = await service.InsertAsync(user);

            // asserts
            id.Should().NotBeEmpty();

            if (!emptyId)
                id.Should().Be(user.Id);

            inserted.Should().BeTrue();

            user.Id = id;
            var dbitem = context.Users.Should().ContainSingle(i => i.Id == id).Which;
            dbitem.Should().BeEquivalentTo(user, o => o.Excluding(i => i.LoginType));
            user.LoginType.Should().Be((Enums.LoginTypes)dbitem.LoginTypeId);
        }
    }
}

﻿using Microsoft.Extensions.Logging;
using SPR.IdentityServer.Abstractions;
using SPR.IdentityServer.Abstractions.Auth;
using SPR.IdentityServer.DataModel;
using System;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Services
{
    public class CredentialsValidator : IUserCredentialsValidator
    {
        private readonly IUsersProvider usersProvider;
        private readonly IPasswordValidator passwordValidator;
        private readonly ILogger<CredentialsValidator> logger;

        public CredentialsValidator(IUsersProvider usersProvider, IPasswordValidator passwordValidator, ILogger<CredentialsValidator> logger)
        {
            this.usersProvider = usersProvider ?? throw new ArgumentNullException(nameof(usersProvider));
            this.passwordValidator = passwordValidator ?? throw new ArgumentNullException(nameof(passwordValidator));
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<(ValidateResult Result, UserModel? User)> ValidateCredentialsAsync(string login, string? password, string? client)
        {
            logger.LogTrace(ValidateUserLogin + " and client '{Client}'", login, client);

            // load user by login
            var user = await usersProvider.LoadAsync(login);
            if (user == null) {
                logger.LogInformation(ValidateUserLogin + " failed because user does not found", login);
                return (ValidateResult.InvalidLoginOrPassword, null);
            }

            // validate user's credentials
            if (!ValidatePassword(user, password)) {
                logger.LogInformation(ValidateUserIdLogin + " failed because password is invalid", user.Id, login);
                return (ValidateResult.InvalidLoginOrPassword, null);
            }

            // if login to specific client
            if (!string.IsNullOrEmpty(client)) {
                logger.LogTrace(ValidateUserIdLoginAndClient + ": check admission to client", user.Id, login, client);
                // assure user can login to this client
                bool clientAdmission = await usersProvider.CanLoginAsync(user.Id, client);
                if (!clientAdmission) {
                    logger.LogInformation(ValidateUserIdLoginAndClient + " failed because user have not admission for this client", user.Id, login, client);
                    return (ValidateResult.ClientNotAllowed, null);
                }
            }

            logger.LogInformation(ValidateUserIdLoginAndClient + " successed", user.Id, login, client);

            // returns success
            return (ValidateResult.Success, user);
        }

        private const string ValidateUserLogin = "Validate credentials for user '{Login}'";
        private const string ValidateUserIdLogin = "Validate credentials for user {UserId} '{Login}'";
        private const string ValidateUserIdLoginAndClient = "Validate credentials for user {UserId} '{Login}' and client '{Client}'";

        private bool ValidatePassword(UserModel user, string? password) => user.LoginType switch {
            Enums.LoginTypes.Local => passwordValidator.Validate(user.Salt, user.PasswordHash, password),
            Enums.LoginTypes.Domain => throw new NotSupportedException(),
            _ => throw new NotImplementedException(),
        };
    }
}

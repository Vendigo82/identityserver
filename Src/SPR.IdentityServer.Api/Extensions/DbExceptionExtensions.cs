﻿using Microsoft.EntityFrameworkCore;

namespace SPR.IdentityServer.Api.Extensions
{
    public static class DbExceptionExtensions
    {
        public static bool IsUniqueKeyViolation(this DbUpdateException e, out string message, out string constraintName)
        {
            if ( e.InnerException is Npgsql.PostgresException se && se.IsUniqueKeyViolation(out constraintName)) {
                message = se.Message;
                return true;
            } else {
                message = string.Empty;
                constraintName = string.Empty;
                return false;
            }
        }

        public static bool IsPrimaryKeyViolation(this DbUpdateException e, out string message)
        {
            if (e.InnerException is Npgsql.PostgresException se && se.IsPrimaryKeyViolation()) {
                message = se.Message;
                return true;
            } else {
                message = string.Empty;
                return false;
            }
        }

        public static bool IsPrimaryKeyViolation(this Npgsql.PostgresException e)
            => e.SqlState == Npgsql.PostgresErrorCodes.UniqueViolation && e.ConstraintName?.EndsWith("_pkey") == true;

        public static bool IsUniqueKeyViolation(this Npgsql.PostgresException e, out string constraintName)
        {
            if (e.SqlState == Npgsql.PostgresErrorCodes.UniqueViolation) {
                constraintName = e.ConstraintName ?? string.Empty;
                return true;
            } else {
                constraintName = e.ConstraintName ?? string.Empty;
                return false;
            }
        }
    }
}

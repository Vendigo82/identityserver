﻿using System.Threading.Tasks;

namespace SPR.IdentityServer.Abstractions
{
    /// <summary>
    /// Provide token for access to user's store api
    /// </summary>
    public interface IUserApiTokenProvider
    {
        /// <summary>
        /// Provide token
        /// </summary>
        /// <returns></returns>
        Task<string> GetTokenAsync();
    }
}

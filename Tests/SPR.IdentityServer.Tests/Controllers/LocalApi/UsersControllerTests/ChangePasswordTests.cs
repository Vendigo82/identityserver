﻿using AutoFixture.Xunit2;
using FluentAssertions.AspNetCore.Mvc;
using Moq;
using SPR.IdentityServer.Api.Abstractions;
using SPR.IdentityServer.Api.DataContract.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Controllers.LocalApi.UsersControllerTests
{
    public class ChangePasswordTests : UsersControllerBaseTests
    {
        [Theory, AutoData]
        public async Task ChangePasswordTest(Guid userId, ChangePasswordRequest request)
        {
            // setup
            var passwordMock = new Mock<IPasswordService>();

            // action
            var result = await controller.ChangePassword(userId, request, passwordMock.Object);

            // asserts
            result.Should().BeOkResult();
            passwordMock.Verify(f => f.SetPasswordAsync(userId, request.Password), Times.Once);
        }
    }
}

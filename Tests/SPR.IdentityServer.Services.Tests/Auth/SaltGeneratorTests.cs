﻿using FluentAssertions;
using Xunit;

namespace SPR.IdentityServer.Services.Auth.Tests
{
    public class SaltGeneratorTests
    {
        readonly SaltGenerator salt = new();

        [Fact]
        public void GenSaltTest()
        {
            // setup

            // action
            var result = salt.GenSalt();

            // asserts
            result.Should()
                .NotBeNullOrWhiteSpace().And.Subject
                .Length.Should().BeGreaterThan(8);
        }

        [Fact]
        public void GenDifferentSaltTest()
        {
            // setup

            // action
            var result1 = salt.GenSalt();
            var result2 = salt.GenSalt();

            // asserts
            result1.Should().NotBe(result2);
        }
    }
}

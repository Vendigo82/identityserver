﻿using AutoFixture.Xunit2;
using FluentAssertions;
using IdentityServer4.Validation;
using Moq;
using SPR.IdentityServer.Abstractions;
using SPR.IdentityServer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Tests.Services
{
    public class ResourceOwnerPasswordValidatorTests
    {
        readonly Mock<IUserCredentialsValidator> validatorMock = new();
        readonly ResourceOwnerPasswordValidator target;

        public ResourceOwnerPasswordValidatorTests()
        {
            target = new(validatorMock.Object);
        }

        [Theory, AutoData]
        public async Task ValidScredentials_ShouldBeSuccess(string username, string password, Guid userId)
        {
            // setup
            validatorMock.Setup(f => f.ValidateCredentialsAsync(username, password, null)).ReturnsAsync((ValidateResult.Success, new DataModel.UserModel { Id = userId }));
            var context = new ResourceOwnerPasswordValidationContext() { 
                Password = password, 
                UserName = username,
                Request = new ValidatedTokenRequest { GrantType = "password" }
            };

            // action
            await target.ValidateAsync(context);

            // asserts
            context.Result.Should().NotBeNull();
            context.Result.Error.Should().BeNull();
            context.Result.Subject.Claims.Should().ContainSingle(i => i.Type == "sub").Which.Value.Should().Be(userId.ToString());

            validatorMock.Verify(f => f.ValidateCredentialsAsync(username, password, null), Times.Once);
        }

        [Theory]
        [InlineAutoData(ValidateResult.ClientNotAllowed)]
        [InlineAutoData(ValidateResult.InvalidLoginOrPassword)]
        public async Task ValidScredentials_ShouldBeSuccessReject(ValidateResult result, string username, string password)
        {
            // setup
            validatorMock.Setup(f => f.ValidateCredentialsAsync(username, password, null)).ReturnsAsync((result, (DataModel.UserModel)null));
            var context = new ResourceOwnerPasswordValidationContext() {
                Password = password,
                UserName = username,
                Request = new ValidatedTokenRequest { GrantType = "password" }
            };

            // action
            await target.ValidateAsync(context);

            // asserts
            context.Result.Should().NotBeNull();
            context.Result.Error.Should().NotBeNull();
            validatorMock.Verify(f => f.ValidateCredentialsAsync(username, password, null), Times.Once);
        }
    }
}

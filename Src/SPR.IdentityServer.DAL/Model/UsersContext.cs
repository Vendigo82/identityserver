﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace SPR.IdentityServer.DAL.Model
{
    public partial class UsersContext : DbContext
    {
        public UsersContext()
        {
        }

        public UsersContext(DbContextOptions<UsersContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<LoginType> LoginTypes { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserClient> UserClients { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasPostgresExtension("pgcrypto")
                .HasAnnotation("Relational:Collation", "English_United States.1252");

            modelBuilder.Entity<Client>(entity =>
            {
                entity.ToTable("client", "identityserver");

                entity.HasComment("Clients (applications)");

                entity.HasIndex(e => e.SystemName, "client_system_name_key")
                    .IsUnique();

                entity.HasIndex(e => e.Title, "client_title_key")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("gen_random_uuid()")
                    .HasComment("PK");

                entity.Property(e => e.Configuration)
                    .HasColumnType("jsonb")
                    .HasColumnName("configuration");

                entity.Property(e => e.IsDisabled)
                    .HasColumnName("is_disabled")
                    .HasComment("Client was disabled");

                entity.Property(e => e.RequirePermission)
                    .IsRequired()
                    .HasColumnName("require_permission")
                    .HasDefaultValueSql("true");

                entity.Property(e => e.SystemName)
                    .IsRequired()
                    .HasColumnName("system_name")
                    .HasComment("Client name for identity server");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasColumnName("title")
                    .HasComment("Client title");
            });

            modelBuilder.Entity<LoginType>(entity =>
            {
                entity.ToTable("login_type", "identityserver");

                entity.HasComment("User login types");

                entity.HasIndex(e => e.SystemName, "login_type_system_name_key")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id")
                    .HasComment("PK");

                entity.Property(e => e.SystemName)
                    .IsRequired()
                    .HasColumnName("system_name")
                    .HasComment("login type system name");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("user", "identityserver");

                entity.HasComment("Users");

                entity.HasIndex(e => e.Name, "idx__users__name");

                entity.HasIndex(e => e.Login, "user_login_key")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("gen_random_uuid()")
                    .HasComment("PK");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("timezone('UTC'::text, now())")
                    .HasComment("Date time when user was created");

                entity.Property(e => e.IsDisabled)
                    .HasColumnName("is_disabled")
                    .HasComment("User was disabled");

                entity.Property(e => e.Login)
                    .IsRequired()
                    .HasColumnName("login")
                    .HasComment("user login");

                entity.Property(e => e.LoginTypeId)
                    .HasColumnName("login_type_id")
                    .HasComment("user login type");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasComment("user name");

                entity.Property(e => e.PasswordHash).HasColumnName("password_hash");

                entity.Property(e => e.Salt).HasColumnName("salt");

                entity.HasOne(d => d.LoginType)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.LoginTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("user_login_type_id_fkey");
            });

            modelBuilder.Entity<UserClient>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.ClientId })
                    .HasName("user_client_pkey");

                entity.ToTable("user_client", "identityserver");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.Property(e => e.ClientId).HasColumnName("client_id");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.UserClients)
                    .HasForeignKey(d => d.ClientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("user_client_client_id_fkey");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserClients)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("user_client_user_id_fkey");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}

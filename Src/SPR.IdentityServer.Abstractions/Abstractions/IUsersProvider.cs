﻿using SPR.IdentityServer.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Abstractions
{
    public interface IUsersProvider
    {
        /// <summary>
        /// Load user by id
        /// </summary>
        /// <param name="id">User's id</param>
        /// <returns>User or null if not found</returns>
        Task<UserModel?> LoadAsync(Guid id);

        /// <summary>
        /// Load user by login
        /// </summary>
        /// <param name="login">User's login</param>
        /// <returns>User or null if not found</returns>
        Task<UserModel?> LoadAsync(string login);

        /// <summary>
        /// Returns true if user can login to client
        /// </summary>
        /// <param name="userId">User's id</param>
        /// <param name="clientName">Client's name</param>
        /// <returns>true if login allowed</returns>
        Task<bool> CanLoginAsync(Guid userId, string? clientName);
    }
}

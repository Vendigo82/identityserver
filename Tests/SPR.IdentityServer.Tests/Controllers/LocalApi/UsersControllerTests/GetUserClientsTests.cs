﻿using AutoFixture.Xunit2;
using FluentAssertions.AspNetCore.Mvc;
using Moq;
using SPR.IdentityServer.Api.DataContract.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Controllers.LocalApi.UsersControllerTests
{
    public class GetUserClientsTests : UsersControllerBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(Guid userId, IEnumerable<ClientDto> clients)
        {
            // setup
            serviceMock.Setup(f => f.GetAuthorizedClientsAsync(userId)).ReturnsAsync(clients);

            // action
            var result = await controller.GetUserClientsAsync(userId);

            // asserts
            result.Should().BeOkObjectResult().WithValueEquivalentTo(new { Items = clients });
            serviceMock.Verify(f => f.GetAuthorizedClientsAsync(userId), Times.Once);
        }
    }
}

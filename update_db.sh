#!/bin/sh

#set -e

# Example:
#   DB_HOST=127.0.0.1 DB_PORT=5432 DB_USER=crecon_owner DB_PASSWORD=qweasd DB_DB=crecon_processes bash ./deploy-services.sh

PSQL="psql -v ON_ERROR_STOP=1 -h $host -p $port -U $usr -d $db"

TMPFILE_BASE=./$(basename "$0").temp.XXXXXXXXXX
TMPFILE=$(mktemp "$TMPFILE_BASE")
ERROR=""

# Truncate temp file
> "$TMPFILE"
echo "begin;" >> "$TMPFILE"
cat `find $(dirname "$0")/sql/ -maxdepth 1 -type f -name "*.sql" | sort` >> "$TMPFILE"
echo "commit;" >> "$TMPFILE"

cat "$TMPFILE" | PGPASSWORD=$psw $PSQL
if [ $? -ne 0 ]; then
    echo ! Update database errors
    ERROR=1
else
    echo Successfully
fi

echo "*** Done"
rm "$TMPFILE"

if [ "$ERROR" != "" ]; then
    exit 51
fi

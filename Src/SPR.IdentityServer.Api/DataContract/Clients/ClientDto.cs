﻿using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SPR.IdentityServer.Api.DataContract.Clients
{
    public class ClientDto
    {
        [JsonProperty(Required = Required.Default)]
        public Guid Id { get; set; }

        [JsonProperty(Required = Required.Always)]
        [MinLength(5)]
        [MaxLength(100)]
        public string SystemName { get; set; } = null!;

        [JsonProperty(Required = Required.Always)]
        [MinLength(5)]
        [MaxLength(150)]
        public string Title { get; set; } = null!;

        [JsonProperty(Required = Required.DisallowNull, DefaultValueHandling = DefaultValueHandling.Populate)]
        [DefaultValue(false)]
        public bool IsDisabled { get; set; }

        [JsonProperty(Required = Required.DisallowNull, DefaultValueHandling = DefaultValueHandling.Populate)]
        [DefaultValue(true)]
        public bool RequirePermission { get; set; }
    }
}

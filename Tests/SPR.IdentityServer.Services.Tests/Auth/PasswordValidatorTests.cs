﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Moq;
using SPR.IdentityServer.Abstractions.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Services.Auth.Tests
{
    public class PasswordValidatorTests
    {
        readonly Mock<IPasswordHasher> hasherMock = new Mock<IPasswordHasher>();
        readonly PasswordValidator validator;

        public PasswordValidatorTests()
        {
            validator = new PasswordValidator(hasherMock.Object);
        }

        [Theory, AutoData]
        public void ValidateSuccessTest(string salt, string password, string hash)
        {
            // setup
            hasherMock.Setup(f => f.HashPassword(salt, password)).Returns(hash);

            // action
            var result = validator.Validate(salt, hash, password);

            // asserts
            result.Should().BeTrue();

            hasherMock.Verify(f => f.HashPassword(salt, password), Times.Once);
            hasherMock.VerifyNoOtherCalls();
        }

        [Theory, AutoData]
        public void ValidateRejectTest(string salt, string password, string hash, string expectedHash)
        {
            // setup
            hasherMock.Setup(f => f.HashPassword(salt, password)).Returns(hash);

            // action
            var result = validator.Validate(salt, expectedHash, password);

            // asserts
            result.Should().BeFalse();

            hasherMock.Verify(f => f.HashPassword(salt, password), Times.Once);
            hasherMock.VerifyNoOtherCalls();
        }

        [Theory]
        [InlineAutoData("", "")]
        [InlineAutoData(null, "")]
        [InlineAutoData("", null)]
        [InlineAutoData(null, null)]
        public void ValidateEmptyTest(string password, string hash, string salt)
        {
            // setup

            // action
            var result = validator.Validate(salt, hash, password);

            // asserts
            result.Should().BeTrue();
            hasherMock.VerifyNoOtherCalls();
        }

        [Theory]
        [InlineAutoData("123", "")]
        [InlineAutoData("", "123")]
        public void ValidateEmptyRejectedTest(string password, string hash, string salt)
        {
            // setup

            // action
            var result = validator.Validate(salt, hash, password);

            // asserts
            result.Should().BeFalse();
        }
    }
}

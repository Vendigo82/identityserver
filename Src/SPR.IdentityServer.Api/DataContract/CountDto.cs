﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Api.DataContract
{
    public class CountDto
    {
        [JsonProperty(Required = Required.Always)]
        public int Count { get; set; }
    }
}

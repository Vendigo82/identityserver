﻿using IdentityModel;
using IdentityServer4.Extensions;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Microsoft.Extensions.Logging;
using SPR.IdentityServer.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Services
{
    public class UsersProfileService : IProfileService
    {
        private readonly IUsersProvider users;
        private readonly ILogger<UsersProfileService> logger;

        public UsersProfileService(IUsersProvider users, ILogger<UsersProfileService> logger)
        {
            this.users = users ?? throw new ArgumentNullException(nameof(users));
            this.logger = logger;
        }

        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            context.LogProfileRequest(logger);

            if (!TryParseUserId(context.Subject.GetSubjectId(), out var userId))
                return;

            var user = await users.LoadAsync(userId);
            if (user == null) {
                logger.LogDebug("User {UserId} not found on get profile data", userId);
                return;
            }

            logger.LogDebug("Fill profile data for user {UserId} '{Login}'", user.Id, user.Login);

            var claims = new List<Claim>();
            foreach (var name in context.RequestedClaimTypes) {
                var value = GetClaimValue(name);
                logger.LogTrace("Claim '{claim}' value for user {UserId} '{Login}' is {value}", name, user.Id, user.Login, value);
                if (value != null)                    
                    claims.Add(new Claim(name, value));               
            }

            context.IssuedClaims = claims;

            string GetClaimValue(string claimName) => claimName switch {
                JwtClaimTypes.Subject => user.Id.ToString(),
                JwtClaimTypes.Name => user.Name,
                "login" => user.Login,
                _ => null
            };
        }

        public async Task IsActiveAsync(IsActiveContext context)
        {
            string userIdString = context.Subject.GetSubjectId();

            logger.LogDebug("Request active status for user {UserId} and client '{Client}'. Caller: {caller}", 
                userIdString, context.Client?.ClientId, context.Caller);

            if (!TryParseUserId(userIdString, out var userId)) {
                context.IsActive = false;
                return;
            }

            if (context.Client != null) {
                context.IsActive = await users.CanLoginAsync(userId, context.Client.ClientId);
                logger.LogDebug("Active status for user '{UserId}' and client '{Client} is {activeResult}", 
                    userId, context.Client.ClientId, context.IsActive);
            } else {
                var user = await users.LoadAsync(userId);
                context.IsActive = user != null;
                logger.LogDebug("Active status for user {UserId} '{Login}' is {activeResult}",
                    userId, user.Login, context.IsActive);
            }
        }

        private bool TryParseUserId(string value, out Guid userId)
        {
            if (!Guid.TryParse(value, out userId)) {
                logger.LogWarning("Can't parse user's id '{UserId}' and treat user as not active", value);
                return false;
            }

            return true;
        }
    }
}

﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using SPR.IdentityServer.Api.DataContract;
using System;

namespace SPR.IdentityServer.Filters
{
    public class ApiExeptionFilter : IExceptionFilter
    {
        private readonly ILogger<ApiExeptionFilter> _logger;

        public ApiExeptionFilter(ILogger<ApiExeptionFilter> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public void OnException(ExceptionContext context)
        {
            var result = GetResult();
            if (result != null) {
                _logger.LogWarning(context.Exception, "Api exception");
                context.Result = result;
                context.ExceptionHandled = true;
            }

            IActionResult GetResult() => context.Exception switch {
                Api.Exceptions.ConstraintViolationException e => new ConflictObjectResult(new ProblemDetails { Title = "Unique violation", Detail = e.Message }),
                Api.Exceptions.ChangePasswordViolationException e => new ConflictObjectResult(new ProblemDetails { Title = "Change password violation", Detail = e.Message }),
                Api.Exceptions.ClientNotFoundException e => new NotFoundObjectResult(new ProblemDetails { Title = "Client does not found", Detail = e.Message }),
                Api.Exceptions.UserNotFoundException e => new NotFoundObjectResult(new ProblemDetails { Title = "User does not found", Detail = e.Message }),
                _ => null,
            };
        }
    }

    public class ApiExceptionFilterAttribute : TypeFilterAttribute
    {
        public ApiExceptionFilterAttribute() : base(typeof(ApiExeptionFilter))
        {
        }
    }
}

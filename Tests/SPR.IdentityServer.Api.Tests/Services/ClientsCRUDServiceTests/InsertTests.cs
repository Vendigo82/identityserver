﻿using AutoFixture.Xunit2;
using FluentAssertions;
using SPR.IdentityServer.Api.DataContract.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Api.Services.ClientsCRUDServiceTests
{
    public class InsertTests : ClientsCRUDServiceBaseTests
    {
        [Theory]
        [InlineAutoData(true)]
        [InlineAutoData(false)]
        public async Task InsertSuccessTest(bool emptyId, ClientDto client)
        {
            // setup
            if (emptyId)
                client.Id = Guid.Empty;

            // action
            var (id, inserted) = await service.InsertAsync(client);

            // asserts
            id.Should().NotBeEmpty();
            if (!emptyId)
                id.Should().Be(client.Id);

            inserted.Should().BeTrue();

            client.Id = id;
            context.Clients.Should()
                .ContainSingle(i => i.Id == id).Which.Should()
                .BeEquivalentTo(client);
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;

namespace SPR.IdentityServer.Controllers.LocalApi
{
    [Route("api/[controller]")]
    [ApiController]
    public class InfoController : ControllerBase
    {
        [HttpGet("/api/version")]
        public IActionResult Version()
        {
            var version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            return Ok(new { Version = version });
        }
    }
}

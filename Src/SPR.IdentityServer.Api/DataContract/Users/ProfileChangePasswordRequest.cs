﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Api.DataContract.Users
{
    public class ProfileChangePasswordRequest : ChangePasswordRequest
    {
        /// <summary>
        /// Old user's password. Allow null
        /// </summary>
        [JsonProperty(Required = Required.AllowNull)]
        public string? OldPassword { get; set; }
    }
}

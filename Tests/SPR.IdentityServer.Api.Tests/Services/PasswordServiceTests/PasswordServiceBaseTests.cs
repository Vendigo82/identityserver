﻿using Moq;
using SPR.IdentityServer.Abstractions.Auth;
using SPR.IdentityServer.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Api.Services.PasswordServiceTests
{
    public class PasswordServiceBaseTests
    {
        protected readonly UsersContext context;
        protected readonly MockRepository mockRepository = new(MockBehavior.Default);
        protected readonly Mock<IPasswordHasher> hasherMock;
        protected readonly Mock<ISaltGenerator> saltMock;
        protected readonly Mock<IPasswordValidator> passwordValidatorMock;
        protected readonly PasswordService service;

        public PasswordServiceBaseTests()
        {
            context = Tools.InitMemoryDd();
            hasherMock = mockRepository.Create<IPasswordHasher>();
            saltMock = mockRepository.Create<ISaltGenerator>();
            passwordValidatorMock = mockRepository.Create<IPasswordValidator>();
            service = new PasswordService(context, hasherMock.Object, saltMock.Object, passwordValidatorMock.Object);
        }
    }
}

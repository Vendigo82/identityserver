﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using SPR.IdentityServer.Api.Abstractions;
using SPR.IdentityServer.Api.DataContract;
using SPR.IdentityServer.Api.DataContract.Clients;
using SPR.IdentityServer.Filters;
using System;
using System.Net.Mime;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Controllers.LocalApi
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ProducesErrorResponseType(typeof(ProblemDetails))]
    [ApiExceptionFilter]
    public class ClientsController : ControllerBase
    {
        private readonly IClientsCRUDService _service;
        private readonly ILogger<ClientsController> _logger;

        public const string GetClientByIdActionName = "GetClient";

        public ClientsController(IClientsCRUDService service, ILogger<ClientsController> logger)
        {
            _service = service ?? throw new ArgumentNullException(nameof(service));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        /// <summary>
        /// Request all clients
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        [Authorize(AuthPolicy.ReadPolicy)]
        [ProducesResponseType(typeof(ListContainer<ClientDto>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetListAsync()
        {
            _logger.LogDebug("Request all clients");

            var list = await _service.GetListAsync();
            return Ok(new ListContainer<ClientDto> { 
                Items = list
            });
        }

        /// <summary>
        /// Get client by id
        /// </summary>
        /// <param name="id">Client's id</param>
        /// <returns>Client info</returns>
        /// <response code="404">Client does not found</response>
        [HttpGet("{id}", Name = GetClientByIdActionName)]
        [Authorize(AuthPolicy.ReadPolicy)]
        [ProducesResponseType(typeof(ClientDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetClientAsync([FromRoute] Guid id)
        {
            _logger.LogDebug("Request single client with id {ClientId}", id);
            var client = await _service.GetAsync(id);
            if (client == null) {
                _logger.LogDebug("Client with id {ClientId} does not found", id);
                return NotFound(new ProblemDetails { Title = "Client not found" });
            }

            _logger.LogDebug("Client with id {ClientId} is {@data}", id, client);
            return Ok(client);
        }

        /// <summary>
        /// Create new client
        /// </summary>
        /// <remarks>
        /// If Id is empty or absent, then generate new id on server side
        /// </remarks>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <response code="201">Item was successfully inserted</response>
        /// <response code="409">There are another client with this name</response>
        [HttpPut]
        [Authorize(AuthPolicy.WriteClientsPolicy)]        
        [ProducesResponseType(typeof(ObjectIdDto), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<IActionResult> InsertAsync(ClientDto model)
        {
            _logger.LogDebug("Inserting new client '{Client}': {@data}", model.SystemName, model);
            var (id, inserted) = await _service.InsertAsync(model);
            if (inserted)
                _logger.LogInformation("New client was inserted with id={ClientId} name='{Client}', data: {@data}", id, model.SystemName, model);
            else
                _logger.LogDebug("Client with id={ClientId} name='{Client}' already exists, data: {@data}", id, model.SystemName, model);
            return CreatedAtAction(GetClientByIdActionName, new { id }, new ObjectIdDto { Id = id });
        }

        /// <summary>
        /// Update existed client
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <response code="200">Item was successfully updated</response>
        /// <response code="400">Require client's id</response>
        /// <response code="404">Item not found</response>
        /// <response code="409">There are another client with this name</response>
        [HttpPost]
        [Authorize(AuthPolicy.WriteClientsPolicy)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        [Consumes(MediaTypeNames.Application.Json)]
        public async Task<IActionResult> UpdateAsync(ClientDto model)
        {
            _logger.LogDebug("Updating client with id {ClientId}, name='{Client}': {@data}", model.Id, model.SystemName, model);
            if (model.Id == Guid.Empty)                
                return BadRequest(new ProblemDetails { Title = "Client's id is required" });

            await _service.UpdateAsync(model);
            _logger.LogInformation("Client with id={ClientId}, name='{Client}' was updated: {@data}", model.Id, model.SystemName, model);
            return Ok();
        }

        /// <summary>
        /// Get client configuration
        /// </summary>
        /// <param name="clientId">client's id</param>
        /// <returns></returns>
        /// <response code="200">Client has configuration</response>
        /// <response code="204">Client exists but configuration not set</response>
        /// <response code="404">Client not found</response>
        [HttpGet("{clientId}/configuration")]
        [Authorize(AuthPolicy.WriteClientsPolicy)]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(JObject))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> ConfigurationGetAsync([FromRoute] Guid clientId)
        {
            var configuration = await _service.GetConfigurationAsync(clientId);
            if (configuration == null)
                return NoContent();

            return Ok(configuration);
        }

        /// <summary>
        /// Get client configuration
        /// </summary>
        /// <param name="clientId">client's id</param>
        /// <param name="configuration">client's configuration</param>
        /// <returns></returns>
        /// <response code="200">Client has configuration</response>
        /// <response code="204">Client exists but configuration not set</response>
        /// <response code="404">Client not found</response>
        [HttpPost("{clientId}/configuration")]
        [Authorize(AuthPolicy.WriteClientsPolicy)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> ConfigurationUpdateAsync([FromRoute] Guid clientId, [FromBody] JRaw configuration)
        {
            await _service.UpdateConfiguratinAsync(clientId, configuration);
            return Ok();
        }


    }
}

﻿using AutoFixture.Xunit2;
using FluentAssertions;
using FluentAssertions.Json;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SPR.IdentittyServer.IntegrationTests.SwaggerClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.IntegrationTests
{
    public class ClientsTests : BaseTests
    {
        private readonly List<Guid> clients = new();

        public ClientsTests(WebServiceTestFixture fixture) : base(fixture)
        {
        }

        public override void Dispose()
        {
            foreach (var c in clients) {
                var client = Context.Clients.Where(i => i.Id == c).FirstOrDefault();
                if (client != null) {
                    Context.Clients.Remove(client);
                    Context.SaveChanges();
                }
            }

            base.Dispose();
        }

        [Fact]
        public async Task GetAllTest()
        {
            // setup

            // action
            var response = await fixture.HttpClient.GetAsync("api/clients");

            // asserts
            response.StatusCode.Should().Be(System.Net.HttpStatusCode.OK);
            var body = JToken.Parse(await response.Content.ReadAsStringAsync());
            body.Should()
                .HaveElement("items").Which.Should()
                .BeOfType<JArray>().And
                .HaveCount(Context.Clients.Count()).And.Subject
                .First.Should()
                    .HaveElement("id").And
                    .HaveElement("systemName").And
                    .HaveElement("title").And
                    .HaveElement("isDisabled").And
                    .HaveElement("requirePermission");
        }

        [Fact]
        public async Task GetItemTest()
        {
            // setup
            var dbitem = Context.Clients.First();

            // action
            var response = await fixture.HttpClient.GetAsync($"api/clients/{dbitem.Id}");

            // asserts
            response.StatusCode.Should().Be(System.Net.HttpStatusCode.OK);
            var jo = JObject.Parse(await response.Content.ReadAsStringAsync());
            //response.Should().NotBeNull();
            //dbitem.Should().BeEquivalentTo(response);
            dbitem.Should().BeEquivalentTo(new {
                Id = (Guid)jo["id"],
                SystemName = (string)jo["systemName"],
                Title = (string)jo["title"],
                IsDisabled = (bool)jo["isDisabled"],
                RequirePermission = (bool)jo["requirePermission"]
            });
        }

        [Theory, AutoData]
        public async Task CRUDTest(Api.DataContract.Clients.ClientDto insertClient, Api.DataContract.Clients.ClientDto updateClient)
        {
            // setup
            updateClient.Id = insertClient.Id;
            clients.Add(insertClient.Id);

            // action
            var responsePut = await fixture.HttpClient.PutAsJsonAsync("api/clients", insertClient);
            var responseGet = await fixture.HttpClient.GetFromJsonAsync<Api.DataContract.Clients.ClientDto>($"api/clients/{insertClient.Id}");
            var responsePost = await fixture.HttpClient.PostAsJsonAsync("api/clients", updateClient);

            // asserts
            responsePut.StatusCode.Should().Be(System.Net.HttpStatusCode.Created);
            responsePost.StatusCode.Should().Be(System.Net.HttpStatusCode.OK);

            var putId = (Guid)JToken.Parse(await responsePut.Content.ReadAsStringAsync()).Should().HaveElement("id").Which;
            putId.Should().Be(insertClient.Id);

            responseGet.Should().BeEquivalentTo(insertClient);

            Context.Clients.Where(i => i.Id == updateClient.Id).Should()
                .ContainSingle().Which.Should()
                .BeEquivalentTo(updateClient);
        }

        [Theory, AutoData]
        public async Task DefaultValuesShouldBeAsExpectedTest(Api.DataContract.Clients.ClientDto insertClient)
        {
            // setup
            clients.Add(insertClient.Id);
            insertClient.RequirePermission = true;
            insertClient.IsDisabled = false;

            // action
            var responsePut = await fixture.HttpClient.PutAsJsonAsync("api/clients", new { 
                id = insertClient.Id,
                systemName = insertClient.SystemName,
                title = insertClient.Title
            });

            // asserts
            responsePut.StatusCode.Should().Be(System.Net.HttpStatusCode.Created);
            Context.Clients.Where(i => i.Id == insertClient.Id).Should()
                .ContainSingle().Which.Should()
                .BeEquivalentTo(insertClient);
        }

        [Theory, AutoData]
        public async Task IdempotentInsertTest(Api.DataContract.Clients.ClientDto client)
        {
            // setup
            clients.Add(client.Id);

            // action
            var responsePut1 = await fixture.HttpClient.PutAsJsonAsync("api/clients", client);
            var responsePut2 = await fixture.HttpClient.PutAsJsonAsync("api/clients", client);

            // asserts
            responsePut1.StatusCode.Should().Be(System.Net.HttpStatusCode.Created);
            responsePut2.StatusCode.Should().Be(System.Net.HttpStatusCode.Created);

            var id1 = (Guid)JToken.Parse(await responsePut1.Content.ReadAsStringAsync()).Should().HaveElement("id").Which;
            var id2 = (Guid)JToken.Parse(await responsePut2.Content.ReadAsStringAsync()).Should().HaveElement("id").Which;
            id1.Should().Be(id2).And.Be(client.Id);
        }

        [Theory, AutoData]
        public async Task SystemNameViolationTest(ClientDto client1, ClientDto client2)
        {
            // setup
            clients.Add(client1.Id);
            clients.Add(client2.Id);
            client2.SystemName = client1.SystemName;

            // action
            var responsePut1 = await fixture.HttpClient.PutAsJsonAsync("api/clients", client1);
            var responsePut2 = await fixture.HttpClient.PutAsJsonAsync("api/clients", client2);

            // asserts
            responsePut1.StatusCode.Should().Be(System.Net.HttpStatusCode.Created);
            responsePut2.StatusCode.Should().Be(System.Net.HttpStatusCode.Conflict);
        }

        [Fact]
        public async Task ConfigurationGet_ShouldBeOk_Test()
        {
            // setup
            var dbclient = await Context.Clients.FirstAsync(c => c.Configuration != null);

            // action
            var response = await fixture.HttpClient.GetAsync($"api/clients/{dbclient.Id}/configuration");

            // asserts
            response.Should().Be200Ok();
            var body = await response.Content.ReadAsStringAsync();
            body.Should().Be(dbclient.Configuration);
        }

        [Fact]
        public async Task ConfigurationGet_WitchNull_ShouldBeNoContent_Test()
        {
            // setup
            var dbclient = await Context.Clients.FirstOrDefaultAsync(c => c.Configuration == null);
            if (dbclient == null)
            {
                var entry = Context.Clients.Add(new DAL.Model.Client { Id = Guid.NewGuid(), SystemName = Guid.NewGuid().ToString(), Title = Guid.NewGuid().ToString() });
                await Context.SaveChangesAsync();
                dbclient = entry.Entity;
                clients.Add(dbclient.Id);
            }

            // action
            var response = await fixture.HttpClient.GetAsync($"api/clients/{dbclient.Id}/configuration");

            // asserts
            response.Should().Be204NoContent();
        }

        [Fact]
        public async Task ConfigurationUpdateTest()
        {
            // setup
            var dbclient = await Context.Clients.FirstOrDefaultAsync(c => c.Configuration == null);
            if (dbclient == null)
            {
                var entry = Context.Clients.Add(new DAL.Model.Client { Id = Guid.NewGuid(), SystemName = Guid.NewGuid().ToString(), Title = Guid.NewGuid().ToString() });
                await Context.SaveChangesAsync();
                dbclient = entry.Entity;
                clients.Add(dbclient.Id);
            }

            var confBody = new { AccessTokenLifetime = "128" };

            // action
            var response = await fixture.HttpClient.PostAsJsonAsync($"api/clients/{dbclient.Id}/configuration", confBody);

            // asserts
            response.Should().Be200Ok();
            await Context.Entry(dbclient).ReloadAsync();
            dbclient.Configuration.Should().NotBeNullOrEmpty();
            var jo = JObject.Parse(dbclient.Configuration);
            jo.Should().HaveElement("accessTokenLifetime").Which.Value<string>().Should().Be(confBody.AccessTokenLifetime, jo.ToString());
        }

    }
}

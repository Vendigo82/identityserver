﻿
namespace SPR.IdentityServer.Abstractions.Auth
{
    /// <summary>
    /// Provide hash password algorithm
    /// </summary>
    public interface IPasswordHasher
    {
        /// <summary>
        /// Hash password
        /// </summary>
        /// <param name="salt">Salt</param>
        /// <param name="password">password</param>
        /// <returns>hash of password</returns>
        public string HashPassword(string salt, string password);
    }
}

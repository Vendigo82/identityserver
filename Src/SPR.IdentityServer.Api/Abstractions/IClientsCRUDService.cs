﻿using Newtonsoft.Json.Linq;
using SPR.IdentityServer.Api.DataContract.Clients;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Api.Abstractions
{
    public interface IClientsCRUDService
    {
        /// <summary>
        /// Get list of clients
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<ClientDto>> GetListAsync();

        /// <summary>
        /// Get single client by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Client or null if not found</returns>
        public Task<ClientDto?> GetAsync(Guid id);

        /// <summary>
        /// Insert new client
        /// </summary>
        /// <param name="client"></param>
        /// <returns>inserted client id or null if there are constraint violation</returns>
        /// <exception cref="Exceptions.ConstraintViolationException">constraint violation</exception>
        public Task<(Guid Id, bool Inserted)> InsertAsync(ClientDto client);

        /// <summary>
        /// Update client
        /// </summary>
        /// <param name="client"></param>
        /// <returns>true if record was updated</returns>
        /// <exception cref="Exceptions.ConstraintViolationException">constraint violation</exception>
        /// <exception cref="Exceptions.ClientNotFoundException">client does not found</exception>
        public Task UpdateAsync(ClientDto client);

        /// <summary>
        /// Get client configuration
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="Exceptions.ClientNotFoundException">client does not found</exception>
        public Task<JRaw?> GetConfigurationAsync(Guid id);

        /// <summary>
        /// Update client configuration
        /// </summary>
        /// <param name="id"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public Task UpdateConfiguratinAsync(Guid id, JRaw configuration);
    }
}

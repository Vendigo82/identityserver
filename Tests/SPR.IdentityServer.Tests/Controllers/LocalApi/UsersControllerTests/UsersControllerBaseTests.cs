﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using SPR.IdentityServer.Api.Abstractions;
using SPR.IdentityServer.Controllers.LocalApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Controllers.LocalApi.UsersControllerTests
{
    public class UsersControllerBaseTests
    {
        protected readonly Mock<IUsersCRUDService> serviceMock = new();
        protected readonly Mock<IOptions<UsersController.Settings>> settingsMock = new();
        protected readonly UsersController controller;

        public UsersControllerBaseTests()
        {
            settingsMock.Setup(f => f.Value).Returns(new UsersController.Settings());
            var logger = new Mock<ILogger<UsersController>>();
            controller = new UsersController(serviceMock.Object, settingsMock.Object, logger.Object);
        }
    }
}

﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using SPR.IdentityServer.Api.Abstractions;
using SPR.IdentityServer.Api.DataContract.Clients;
using SPR.IdentityServer.Api.Exceptions;
using SPR.IdentityServer.Api.Extensions;
using SPR.IdentityServer.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Api.Services
{
    public class ClientsCRUDService : IClientsCRUDService
    {
        private readonly IMapper mapper;
        private readonly UsersContext context;

        public ClientsCRUDService(UsersContext context, IMapper mapper)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<IEnumerable<ClientDto>> GetListAsync()
        {
            return await context.Clients.OrderBy(i => i.SystemName).ProjectTo<ClientDto>(mapper.ConfigurationProvider).ToListAsync();
        }

        public Task<ClientDto?> GetAsync(Guid id)
        {
            return context.Clients.Where(i => i.Id == id).ProjectTo<ClientDto?>(mapper.ConfigurationProvider).SingleOrDefaultAsync();
        }

        public async Task<(Guid Id, bool Inserted)> InsertAsync(ClientDto client)
        {
            var dbitem = mapper.Map<Client>(client);
            dbitem.Id = client.Id;
            await context.Clients.AddAsync(dbitem);

            try {
                await context.SaveChangesAsync();
            } catch (DbUpdateException e) {
                if (e.IsPrimaryKeyViolation(out var _))
                        return (client.Id, false);
                else if (e.IsUniqueKeyViolation(out var msg, out var constraintName))
                        throw new ConstraintViolationException($"There are exists another client with same value for '{constraintName}': {msg}", constraintName, e);
                else
                    throw;
            }

            return (dbitem.Id, true);
        }

        public async Task UpdateAsync(ClientDto client)
        {
            var dbitem = await context.Clients.Where(i => i.Id == client.Id).SingleOrDefaultAsync();
            if (dbitem == null)
                throw new ClientNotFoundException(client.Id);

            mapper.Map(client, dbitem);

            try {
                int cnt = await context.SaveChangesAsync();
            } catch (DbUpdateException e) {
                if (e.IsUniqueKeyViolation(out var msg, out var columnName))
                    throw new ConstraintViolationException($"There are exists another client same value for '{columnName}': {msg}", columnName, e);
                else
                    throw;
            }
        }

        public async Task<JRaw?> GetConfigurationAsync(Guid id)
        {
            var dbitem = await context.Clients.AsNoTracking().Where(i => i.Id == id).SingleOrDefaultAsync();
            if (dbitem == null)
                throw new ClientNotFoundException(id);

            if (dbitem.Configuration == null)
                return null;

            return new JRaw(dbitem.Configuration);
        }
        
        public async Task UpdateConfiguratinAsync(Guid id, JRaw configuration)
        {
            var dbitem = await context.Clients.Where(i => i.Id == id).SingleOrDefaultAsync();
            if (dbitem == null)
                throw new ClientNotFoundException(id);

            dbitem.Configuration = configuration.ToString();
            await context.SaveChangesAsync();
        }
    }
}

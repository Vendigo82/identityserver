﻿using IdentityModel;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace SPR.IdentityServer.IntegrationTests.TestHost
{
    public class TestAuthHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        public ClaimsFactory ClaimsFactory { get; }


        public TestAuthHandler(ClaimsFactory claimsFactory,
            IOptionsMonitor<AuthenticationSchemeOptions> options,
            ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock)
            : base(options, logger, encoder, clock)
        {
            ClaimsFactory = claimsFactory;
        }

        protected override Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            var claims = new [] { 
                "users_api.read", 
                "users_api.write_clients", 
                "users_api.write_users", 
                "users_api.change_password",
                "users_api.profile"
            }.Select(i => new Claim(JwtClaimTypes.Scope, i))
            .ToList();

            claims.Add(new Claim(ClaimTypes.NameIdentifier, ClaimsFactory.UserId.ToString()));
            claims.Add(new Claim(JwtClaimTypes.Subject, ClaimsFactory.UserId.ToString()));

            var identity = new ClaimsIdentity(claims, "Test");
            var principal = new ClaimsPrincipal(identity);
            var ticket = new AuthenticationTicket(principal, "Test");

            var result = AuthenticateResult.Success(ticket);

            return Task.FromResult(result);
        }
    }

}

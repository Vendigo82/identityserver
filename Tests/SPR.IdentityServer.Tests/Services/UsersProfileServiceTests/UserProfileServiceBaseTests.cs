﻿using Microsoft.Extensions.Logging;
using Moq;
using SPR.IdentityServer.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Services.UsersProfileServiceTests
{
    public class UserProfileServiceBaseTests
    {
        protected readonly Mock<IUsersProvider> usersMock = new Mock<IUsersProvider>();
        protected readonly UsersProfileService service;

        public UserProfileServiceBaseTests()
        {
            var logger = new Mock<ILogger<UsersProfileService>>();

            service = new UsersProfileService(usersMock.Object, logger.Object);
        }
    }
}

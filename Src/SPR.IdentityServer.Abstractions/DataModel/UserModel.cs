﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.IdentityServer.DataModel
{
    public class UserModel
    {
        /// <summary>
        /// User's id
        /// </summary>
        public Guid Id { get; init; }

        /// <summary>
        /// User's name
        /// </summary>
        public string? Name { get; init; }

        /// <summary>
        /// User login type
        /// </summary>
        public Enums.LoginTypes LoginType { get; init; }

        /// <summary>
        /// Login
        /// </summary>
        public string Login { get; init; } = null!;

        /// <summary>
        /// Salt. Can be null
        /// </summary>
        public string? Salt { get; init; }

        /// <summary>
        /// Password hash. Can be null, for local user that means without password
        /// </summary>
        public string? PasswordHash { get; init; }
    }
}

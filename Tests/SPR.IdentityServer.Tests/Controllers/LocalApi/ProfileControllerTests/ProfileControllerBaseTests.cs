﻿using IdentityModel;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Moq;
using SPR.IdentityServer.Api.Abstractions;
using SPR.IdentityServer.Controllers.LocalApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Tests.Controllers.LocalApi.ProfileControllerTests
{
    public class ProfileControllerBaseTests
    {
        protected readonly Mock<IUsersCRUDService> usersMock = new();
        protected readonly ProfileController controller;
        protected readonly Guid userId = Guid.NewGuid();

        public ProfileControllerBaseTests()
        {
            controller = new ProfileController(usersMock.Object, Mock.Of<ILogger<ProfileController>>());

            controller.ControllerContext.HttpContext = new DefaultHttpContext() {
                User = new ClaimsPrincipal(new ClaimsIdentity(new[] {
                    new Claim(ClaimTypes.NameIdentifier, userId.ToString()),
                    new Claim(JwtClaimTypes.Subject, userId.ToString())
                }))
            };

        }
    }
}

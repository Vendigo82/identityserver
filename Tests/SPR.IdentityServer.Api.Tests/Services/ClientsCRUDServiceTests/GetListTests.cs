﻿using AutoFixture;
using FluentAssertions;
using SPR.IdentityServer.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Api.Services.ClientsCRUDServiceTests
{
    public class GetListTests : ClientsCRUDServiceBaseTests
    {        
        [Fact]
        public async Task SuccessTest()
        {
            // setup
            context.Clients.AddRange(Enumerable.Range(1, 3).Select(i => fixture.Build<Client>().Without(i => i.UserClients).Create()));
            context.SaveChanges();

            // action
            var result = await service.GetListAsync();

            // asserts
            result.Should().NotBeNull().And.HaveSameCount(context.Clients);
            context.Clients.Should().BeEquivalentTo(result);
        }
    }
}

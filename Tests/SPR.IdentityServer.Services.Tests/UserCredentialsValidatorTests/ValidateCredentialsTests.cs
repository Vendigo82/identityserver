﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Moq;
using SPR.IdentityServer.Abstractions;
using SPR.IdentityServer.Abstractions.Auth;
using SPR.IdentityServer.DataModel;
using SPR.IdentityServer.Services.AutoFixture;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Services.UserCredentialsValidatorTests
{
    public class ValidateCredentialsTests
    {
        readonly MockRepository repository = new(MockBehavior.Default);
        readonly Mock<IUsersProvider> users;
        readonly Mock<IPasswordValidator> passwordValidator;
        readonly CredentialsValidator validator;

        public ValidateCredentialsTests()
        {
            users = repository.Create<IUsersProvider>();
            passwordValidator = repository.Create<IPasswordValidator>();
            var logger = new Mock<ILogger<CredentialsValidator>>();

            validator = new CredentialsValidator(users.Object, passwordValidator.Object, logger.Object);
        }

        [Theory, ISAutoData]
        public async Task SuccessTest(string clientName, UserModel userModel, string password)
        {
            // setup
            users.Setup(f => f.LoadAsync(userModel.Login)).ReturnsAsync(userModel);
            passwordValidator.Setup(f => f.Validate(userModel.Salt, userModel.PasswordHash, password)).Returns(true);
            users.Setup(f => f.CanLoginAsync(userModel.Id, clientName)).ReturnsAsync(true);

            // action
            var (result, user) = await validator.ValidateCredentialsAsync(userModel.Login, password, clientName);

            // asserts
            result.Should().Be(ValidateResult.Success);
            user.Should().BeSameAs(userModel);

            users.Verify(f => f.LoadAsync(userModel.Login), Times.Once);
            passwordValidator.Verify(f => f.Validate(userModel.Salt, userModel.PasswordHash, password), Times.Once);
            users.Verify(f => f.CanLoginAsync(userModel.Id, clientName), Times.Once);
            repository.VerifyNoOtherCalls();
        }

        [Theory]
        [InlineISAutoData(Enums.LoginTypes.Local, true)]
        [InlineISAutoData(Enums.LoginTypes.Local, false)]
        public async Task SuccessWithoutClient(bool emptyClientName, UserModel userModel, string password)
        {
            // setup
            users.Setup(f => f.LoadAsync(userModel.Login)).ReturnsAsync(userModel);
            passwordValidator.Setup(f => f.Validate(userModel.Salt, userModel.PasswordHash, password)).Returns(true);

            // action
            var (result, user) = await validator.ValidateCredentialsAsync(userModel.Login, password, emptyClientName ? string.Empty : null);

            // asserts
            result.Should().Be(ValidateResult.Success);
            user.Should().BeSameAs(userModel);

            users.Verify(f => f.LoadAsync(userModel.Login), Times.Once);
            passwordValidator.Verify(f => f.Validate(userModel.Salt, userModel.PasswordHash, password), Times.Once);
            repository.VerifyNoOtherCalls();
        }

        /// <summary>
        /// Assure only empty password and password hash was returns success result
        /// </summary>
        /// <param name="password"></param>
        /// <param name="passwordHash"></param>
        /// <param name="expectedSuccess"></param>
        /// <returns></returns>
        [Theory]
        [InlineAutoData(null, null)]
        [InlineAutoData(null, "")]
        [InlineAutoData("", null)]
        [InlineAutoData("", "")]
        [InlineAutoData("psw", "hash")]
        [InlineAutoData("psw", "")]
        [InlineAutoData("psw", null)]
        [InlineAutoData("", "hash")]
        [InlineAutoData(null, "hash")]
        public async Task EmptyPasswordTest(string password, string passwordHash)
        {
            // setup
            var userModel = new UserModel { 
                LoginType = Enums.LoginTypes.Local,
                PasswordHash = passwordHash
            };
            users.Setup(f => f.LoadAsync("login")).ReturnsAsync(userModel);
            passwordValidator.Setup(f => f.Validate(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(false);

            // action
            var (result, user) = await validator.ValidateCredentialsAsync("login", password, null);

            // asserts
            passwordValidator.Verify(f => f.Validate(userModel.Salt, userModel.PasswordHash, password), Times.Once);

        }

        [Theory, ISAutoData(Enums.LoginTypes.Domain)]
        public async Task DomainNotSupportedTest(UserModel userModel, string password, string clientName)
        {
            // setup
            users.Setup(f => f.LoadAsync(userModel.Login)).ReturnsAsync(userModel);
            passwordValidator.Setup(f => f.Validate(userModel.Salt, userModel.PasswordHash, password)).Returns(true);            

            // action
            Func<Task> action = () => validator.ValidateCredentialsAsync(userModel.Login, password, clientName);

            // asserts
            await action.Should().ThrowExactlyAsync<NotSupportedException>();

            users.Verify(f => f.LoadAsync(userModel.Login), Times.Once);
            repository.VerifyNoOtherCalls();
        }

        [Theory, ISAutoData]
        public async Task ClientNotAllowedTest(UserModel userModel, string password, string clientName)
        {
            // setup
            users.Setup(f => f.LoadAsync(userModel.Login)).ReturnsAsync(userModel);
            passwordValidator.Setup(f => f.Validate(userModel.Salt, userModel.PasswordHash, password)).Returns(true);
            users.Setup(f => f.CanLoginAsync(userModel.Id, clientName)).ReturnsAsync(false);    //client not allowed

            // action
            var (result, user) = await validator.ValidateCredentialsAsync(userModel.Login, password, clientName);

            // asserts
            result.Should().Be(ValidateResult.ClientNotAllowed);
            user.Should().BeNull();

            users.Verify(f => f.LoadAsync(userModel.Login), Times.Once);
            passwordValidator.Verify(f => f.Validate(userModel.Salt, userModel.PasswordHash, password), Times.Once);
            users.Verify(f => f.CanLoginAsync(userModel.Id, clientName), Times.Once);
            repository.VerifyNoOtherCalls();
        }

        [Theory, AutoData]
        public async Task UserNotFoundTest(string login, string password, string clientName)
        {
            // setup
            users.Setup(f => f.LoadAsync(login)).ReturnsAsync((UserModel)null);

            // action
            var (result, user) = await validator.ValidateCredentialsAsync(login, password, clientName);

            // asserts
            result.Should().Be(ValidateResult.InvalidLoginOrPassword);
            user.Should().BeNull();

            users.Verify(f => f.LoadAsync(login), Times.Once);
            repository.VerifyNoOtherCalls();
        }
    }
}

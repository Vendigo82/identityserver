﻿using AutoFixture.Xunit2;
using FluentAssertions.AspNetCore.Mvc;
using Moq;
using SPR.IdentityServer.Api.DataContract.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Tests.Controllers.LocalApi.ProfileControllerTests
{
    public class GetClientsAsyncTests : ProfileControllerBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(IEnumerable<ClientDto> clients)
        {
            // setup
            usersMock.Setup(f => f.GetAuthorizedClientsAsync(userId)).ReturnsAsync(clients);

            // action
            var result = await controller.GetClientsAsync();

            result.Should().BeOkObjectResult().WithValueEquivalentTo(new { Items = clients });
            usersMock.Verify(f => f.GetAuthorizedClientsAsync(userId), Times.Once);
        }
    }
}

﻿using AutoFixture;
using AutoMapper;
using SPR.IdentityServer.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Api.Services.ClientsCRUDServiceTests
{
    public class ClientsCRUDServiceBaseTests
    {
        protected readonly Fixture fixture = new Fixture();
        protected readonly UsersContext context;
        protected readonly ClientsCRUDService service;

        public ClientsCRUDServiceBaseTests()
        {
            context = Tools.InitMemoryDd();

            var mapper = new Mapper(new MapperConfiguration(c => c.AddProfile<Mapping.MappingProfile>()));

            service = new ClientsCRUDService(context, mapper);
        }
    }
}

﻿using Moq;
using FluentAssertions;
using FluentAssertions.AspNetCore.Mvc;
using SPR.IdentityServer.Api.DataContract.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using AutoFixture.Xunit2;
using SPR.IdentityServer.Api.DataContract;

namespace SPR.IdentityServer.Controllers.LocalApi.ClientControllerTests
{
    public class GetListTests : ClientControllerBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(IEnumerable<ClientDto> clients)
        {
            // setup
            clientsMock.Setup(f => f.GetListAsync()).ReturnsAsync(clients);

            // action
            var result = await controller.GetListAsync();

            // asserts
            result.Should()
                .BeOkObjectResult().Value.Should()
                .BeOfType<ListContainer<ClientDto>>().Which.Items.Should()
                .BeSameAs(clients);

            clientsMock.Verify(f => f.GetListAsync(), Times.Once);
            clientsMock.VerifyNoOtherCalls();
        }
    }
}

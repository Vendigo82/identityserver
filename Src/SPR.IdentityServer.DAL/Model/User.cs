﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SPR.IdentityServer.DAL.Model
{
    public partial class User
    {
        public User()
        {
            UserClients = new HashSet<UserClient>();
        }

        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public bool IsDisabled { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public short LoginTypeId { get; set; }
        public string Salt { get; set; }
        public string PasswordHash { get; set; }

        public virtual LoginType LoginType { get; set; }
        public virtual ICollection<UserClient> UserClients { get; set; }
    }
}

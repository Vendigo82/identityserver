﻿using FluentAssertions;
using FluentAssertions.Json;
using FluentAssertions.Web;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.IntegrationTests
{
    public class IdentityServerTests : IClassFixture<WebApplicationFactory<Startup>>
    {
        readonly WebApplicationFactory<Startup> fixture;
        readonly HttpClient httpClient;

        public IdentityServerTests(WebApplicationFactory<Startup> fixture)
        {
            this.fixture = fixture ?? throw new ArgumentNullException(nameof(fixture));
            httpClient = this.fixture.CreateClient();
        }

        [Fact]
        public async Task WellKnownTest()
        {
            // setup

            // action
            var response = await httpClient.GetAsync("/.well-known/openid-configuration");

            // asserts
            response.Should().Be200Ok();
        }

        [Fact]
        public async Task TokenClientCredentialsTest()
        {
            // setup
            var requestMessage = new HttpRequestMessage(HttpMethod.Post, "/connect/token") {
                Content = new FormUrlEncodedContent(new[] {
                    KeyValuePair.Create("grant_type", "client_credentials"),
                    KeyValuePair.Create("client_id", "test_client"),
                    KeyValuePair.Create("client_secret", "secret"),
                })
            };

            // action
            var response = await httpClient.SendAsync(requestMessage);

            // asserts
            response.Should().Be200Ok();
            var jtoken = JToken.Parse(await response.Content.ReadAsStringAsync());
            jtoken.Should()
                .HaveElement("access_token").And
                .HaveElement("expires_in").And
                .HaveElement("token_type").And
                .HaveElement("scope");
        }

        [Theory]
        [InlineData("", true)]
        [InlineData("invalid_password", false)]
        public async Task TokenPasswordTest(string password, bool expectedSuccess)
        {
            // setup
            var requestMessage = new HttpRequestMessage(HttpMethod.Post, "/connect/token") {
                Content = new FormUrlEncodedContent(new[] {
                    KeyValuePair.Create("grant_type", "password"),
                    KeyValuePair.Create("client_id", "test_password_client"),
                    KeyValuePair.Create("client_secret", "secret"),
                    KeyValuePair.Create("username", "sa"),
                    KeyValuePair.Create("password", password),
                })
            };

            // action
            var response = await httpClient.SendAsync(requestMessage);

            // asserts
            if (expectedSuccess)
            {
                response.Should().Be200Ok();
                var jtoken = JToken.Parse(await response.Content.ReadAsStringAsync());
                jtoken.Should()
                    .HaveElement("access_token").And
                    .HaveElement("expires_in").And
                    .HaveElement("token_type").And
                    .HaveElement("scope");
            }
            else
                response.Should().Be400BadRequest();
        }
    }
}

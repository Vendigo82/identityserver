﻿using AutoFixture.Xunit2;
using FluentAssertions;
using FluentAssertions.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc;
using Moq;
using SPR.IdentityServer.Api.DataContract;
using SPR.IdentityServer.Api.DataContract.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Controllers.LocalApi.UsersControllerTests
{
    public class GetTests : UsersControllerBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(UserDto user)
        {
            // setup
            serviceMock.Setup(f => f.GetAsync(user.Id)).ReturnsAsync(user);

            // action
            var result = await controller.GetUserAsync(user.Id);

            // asserts
            result.Should().BeOkObjectResult().WithValueEquivalentTo(user);
            serviceMock.Verify(f => f.GetAsync(user.Id), Times.Once);
        }

        [Theory, AutoData]
        public async Task NotFoundTest(Guid userId)
        {
            // setup
            serviceMock.Setup(f => f.GetAsync(userId)).ReturnsAsync((UserDto)null);

            // action
            var result = await controller.GetUserAsync(userId);

            // asserts
            result.Should().BeNotFoundObjectResult().Value.Should().BeOfType<ProblemDetails>();
            serviceMock.Verify(f => f.GetAsync(userId), Times.Once);
        }
    }
}

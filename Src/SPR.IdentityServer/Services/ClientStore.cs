﻿using IdentityServer4.Stores;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SPR.IdentityServer.DAL.Model;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Services
{
    public class ClientStore : IClientStore
    {
        private readonly UsersContext _context;

        public ClientStore(UsersContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<IdentityServer4.Models.Client> FindClientByIdAsync(string clientId)
        {
            var dbClient = await _context.Clients.Where(i => i.SystemName == clientId).SingleOrDefaultAsync();
            if (dbClient == null)
                return null;

            var client = CreateClient();
            client.ClientId = dbClient.SystemName;
            client.ClientName = dbClient.Title;
            client.Enabled = !dbClient.IsDisabled;
            return client;            

            IdentityServer4.Models.Client CreateClient()
            {
                if (dbClient.Configuration == null)
                    return new IdentityServer4.Models.Client();

                return JsonConvert.DeserializeObject<IdentityServer4.Models.Client>(dbClient.Configuration);
            }
        }
    }
}

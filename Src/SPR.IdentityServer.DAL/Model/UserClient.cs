﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SPR.IdentityServer.DAL.Model
{
    public partial class UserClient
    {
        public Guid UserId { get; set; }
        public Guid ClientId { get; set; }

        public virtual Client Client { get; set; }
        public virtual User User { get; set; }
    }
}

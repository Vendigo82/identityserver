﻿using AutoFixture.Xunit2;
using FluentAssertions;
using FluentAssertions.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc;
using Moq;
using SPR.IdentityServer.Api.DataContract;
using SPR.IdentityServer.Api.DataContract.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Controllers.LocalApi.ClientControllerTests
{
    public class InsertTests : ClientControllerBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(ClientDto client, Guid id)
        {
            // setup
            clientsMock.Setup(f => f.InsertAsync(client)).ReturnsAsync((id, true));

            // action
            var response = await controller.InsertAsync(client);

            // asserts
            response.Should()
                .BeCreatedAtActionResult()
                .WithActionName(ClientsController.GetClientByIdActionName)
                .WithRouteValue("id", id)
                .WithValueEquivalentTo(new { Id = id });

            clientsMock.Verify(f => f.InsertAsync(client), Times.Once);
            clientsMock.VerifyNoOtherCalls();
        }
    }
}

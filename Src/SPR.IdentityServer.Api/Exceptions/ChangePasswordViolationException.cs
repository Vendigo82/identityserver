﻿using System;

namespace SPR.IdentityServer.Api.Exceptions
{
    public class ChangePasswordViolationException : Exception
    {
        public ChangePasswordViolationException(string? message) : base(message)
        {
        }
    }
}

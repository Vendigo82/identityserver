﻿using AutoMapper;
using Moq;
using SPR.IdentityServer.Abstractions;
using SPR.IdentityServer.DAL.Model;
using SPR.IdentityServer.Services.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Services.DatabaseUsersProviderTests
{
    public class DatabaseUsersProviderBaseTests
    {
        protected readonly UsersContext context;
        protected readonly DatabaseUsersProvider store;

        public DatabaseUsersProviderBaseTests()
        {
            context = Tools.InitMemoryDd();
            var mapper = new Mapper(new MapperConfiguration(c => c.AddProfile<EFMappingProfile>()));
            store = new DatabaseUsersProvider(context, mapper);

            InitDb();
        }

        private void InitDb()
        {
            context.Users.Add(new User {
                Id = Guid.NewGuid(),
                IsDisabled = false,
                Login = "alice",
                LoginTypeId = (short)Enums.LoginTypes.Local,
                Name = Guid.NewGuid().ToString(),
                Salt = Guid.NewGuid().ToString(),
                PasswordHash = Guid.NewGuid().ToString()
            });
            context.SaveChanges();
        }
    }
}

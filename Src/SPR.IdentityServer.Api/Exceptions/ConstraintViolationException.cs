﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Api.Exceptions
{
    public class ConstraintViolationException : Exception
    {
        public string ConstraintName { get; }

        public ConstraintViolationException(string? message, string constraintName, Exception innerException) : base(message, innerException)
        {
            ConstraintName = constraintName;
        }
    }
}

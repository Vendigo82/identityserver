create schema if not exists identityserver;

select utils.execute($$
    create table identityserver.login_type (
        id smallint primary key,

        system_name text not null,

        unique (system_name)
    );

    comment on table identityserver.login_type is 'User login types';
    comment on column identityserver.login_type.id is 'PK';
    comment on column identityserver.login_type.system_name is 'login type system name';
$$) where not utils.table_exists('identityserver', 'login_type');


select utils.execute($$
    create table identityserver.client (
        id uuid primary key default public.gen_random_uuid(),

        system_name text not null,
		is_disabled boolean not null default false,
		title text not null,

        unique (system_name),
		unique (title)
    );

    comment on table identityserver.client is 'Clients (applications)';
    comment on column identityserver.client.id is 'PK';
    comment on column identityserver.client.system_name is 'Client name for identity server';
	comment on column identityserver.client.is_disabled is 'Client was disabled';
	comment on column identityserver.client.title is 'Client title';
$$) where not utils.table_exists('identityserver', 'client');

select utils.execute($$
    alter table identityserver.client
	add require_permission boolean not null default true;
$$) where not utils.column_exists('identityserver', 'client', 'require_permission');

select utils.execute($$
    alter table identityserver.client
	add configuration jsonb null;
$$) where not utils.column_exists('identityserver', 'client', 'configuration');

select utils.execute($$
    create table identityserver.user (
        id uuid primary key default public.gen_random_uuid(),

        created_at timestamp not null default (now() at time zone 'UTC'),
		is_disabled boolean not null default false,
		name text null,
		login text not null,
		login_type_id smallint not null,
		salt text null,
		password_hash text null,

        unique (login),
		
		foreign key (login_type_id) references identityserver.login_type (id)

    );

	--create index idx__user__login_type_id on identityserver.user (login_type_id);
	create index idx__user__name on identityserver.user (name);


    comment on table identityserver.user is 'Users';
    comment on column identityserver.user.id is 'PK';
    comment on column identityserver.user.created_at is 'Date time when user was created';
	comment on column identityserver.user.is_disabled is 'User was disabled';
	comment on column identityserver.user.name is 'user name';
	comment on column identityserver.user.login is 'user login';
	comment on column identityserver.user.login_type_id is 'user login type';
$$) where not utils.table_exists('identityserver', 'user');


select utils.execute($$
    create table identityserver.user_client (
        user_id uuid,
		client_id uuid,
		primary key (user_id, client_id),
		
		foreign key (user_id) references identityserver.user (id),
		foreign key (client_id) references identityserver.client (id)
    );
$$) where not utils.table_exists('identityserver', 'user_client');

select utils.execute($$
    insert into identityserver.login_type (id, system_name) values
        (1, 'local'),
        (2, 'domain');
$$) where not exists (select * from identityserver.login_type);
﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using SPR.IdentittyServer.IntegrationTests.SwaggerClient;
using SPR.IdentityServer.IntegrationTests.TestHost;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xunit;

[assembly: CollectionBehavior(DisableTestParallelization = true)]

namespace SPR.IdentityServer.IntegrationTests
{
    public class WebServiceTestFixture
    {
        /// <summary>
        /// Web-service factory
        /// </summary>
        public WebApplicationFactory<Startup> Factory { get; }

        /// <summary>
        /// Web-service http client
        /// </summary>
        public HttpClient HttpClient { get; }

        public ApiClient Client { get; }

        /// <summary>
        /// Service provider
        /// </summary>
        public IServiceProvider ServiceProvider { get; private set; }

        public WebServiceTestFixture()
        {
            Factory = new WebApplicationFactory<Startup>();

            var tmp = Factory
                .WithWebHostBuilder(builder => {
                    builder.ConfigureTestServices(services => {
                        services
                            .AddAuthentication(x => {
                                x.DefaultAuthenticateScheme = "Test";
                                x.DefaultChallengeScheme = "Test";
                            })
                            .AddScheme<AuthenticationSchemeOptions, TestAuthHandler>("Test", options => {
                            });

                        services.AddAuthorization(options => {
                            options.ConfigureLocalApiPolicies("Test");
                        });

                        services.AddSingleton<ClaimsFactory>();
                    });
                }).Server;

            HttpClient = tmp.CreateClient();
            Client = new ApiClient(HttpClient);
            ServiceProvider = tmp.Services;

            HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Test");            
        }
    }

}

﻿//using IdentityServer4.Extensions;
//using Microsoft.AspNetCore.Builder;
//using Microsoft.AspNetCore.Http;
//using Microsoft.Extensions.Options;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;

//namespace SPR.IdentityServer.Middleware
//{
//    public class RewriteIdetityServerUrlMiddleware
//    {
//        private readonly RequestDelegate _next;
//        private readonly IOptions<Settings> _options;

//        public RewriteIdetityServerUrlMiddleware(RequestDelegate next, IOptions<Settings> options)
//        {
//            _next = next;
//            _options = options;
//        }

//        public async Task InvokeAsync(HttpContext context)
//        {
//            if (_options.Value.PublicOrigin != null)
//                context.SetIdentityServerOrigin(_options.Value.PublicOrigin);
//            else if (_options.Value.SslOffload)
//                context.Request.Scheme = "https";

//            // Call the next delegate/middleware in the pipeline
//            await _next(context);
//        }

//        public class Settings
//        {
//            /// <summary>
//            /// Rewrite http to https
//            /// </summary>
//            public bool SslOffload { get; set; }

//            /// <summary>
//            /// Rewrite public url
//            /// </summary>
//            public string PublicOrigin { get; set; }
//        }
//    }

//    public static class RewriteIdetityServerUrlMiddlewareExtensions
//    {
//        public static IApplicationBuilder UseRewriteIdetityServerUrl(this IApplicationBuilder builder)
//        {
//            return builder.UseMiddleware<RewriteIdetityServerUrlMiddleware>();
//        }
//    }
//}

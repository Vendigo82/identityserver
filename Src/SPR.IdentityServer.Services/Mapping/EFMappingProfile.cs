﻿using AutoMapper;
using SPR.IdentityServer.DAL.Model;
using SPR.IdentityServer.DataModel;

namespace SPR.IdentityServer.Services.Mapping
{
    public class EFMappingProfile : Profile
    {
        public EFMappingProfile()
        {
            CreateMap<short, Enums.LoginTypes>().ConvertUsing(v => (Enums.LoginTypes)v);            

            CreateMap<User, UserModel>()
                .ForMember(d => d.LoginType, m => m.MapFrom(s => s.LoginTypeId));
        }
    }
}

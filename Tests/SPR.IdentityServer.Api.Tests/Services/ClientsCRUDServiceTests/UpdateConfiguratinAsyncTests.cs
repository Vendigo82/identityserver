﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Api.Services.ClientsCRUDServiceTests
{
    public class UpdateConfiguratinAsyncTests : ClientsCRUDServiceBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessUpdateTest(Guid clientId)
        {
            // setup
            var json = "{ \"field1\": 123 }";
            var dbitem = context.Clients.Add(new DAL.Model.Client {
                Id = clientId
            });
            await context.SaveChangesAsync();

            // action
            await service.UpdateConfiguratinAsync(clientId, new JRaw(json));

            // asserts
            await dbitem.ReloadAsync();
            dbitem.Entity.Configuration.Should().Be(json);
        }

        [Theory, AutoData]
        public async Task ClientNotFoundTest(Guid clientId)
        {
            // setup
            var json = "{ \"field1\": 123 }";

            // action
            Func<Task> action = () => service.UpdateConfiguratinAsync(clientId, new JRaw(json));

            // asserts
            await action.Should().ThrowExactlyAsync<Exceptions.ClientNotFoundException>();
        }
    }
}

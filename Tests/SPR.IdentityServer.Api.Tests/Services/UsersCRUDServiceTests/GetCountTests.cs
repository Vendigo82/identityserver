﻿using AutoFixture;
using FluentAssertions;
using SPR.IdentityServer.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Api.Services.UsersCRUDServiceTests
{
    public class GetCountTests : UsersCRUDServiceBaseTests
    {
        public GetCountTests()
        {
            context.Users.AddRange(Enumerable.Range(1, 10).Select(i => fixture.Create<User>()));
            context.SaveChanges();
        }

        [Fact]
        public async Task GetCountTest()
        {
            // setup 

            // action
            var result = await service.GetCountAsync(null);

            // asserts
            result.Should().Be(context.Users.Count());
        }

        [Fact]
        public async Task FilterByLoginTest()
        {
            // setup
            var dbitem = context.Users.First();
            var filter = dbitem.Login[5..10];

            // action
            var result = await service.GetCountAsync(filter);

            // asserts
            result.Should().NotBe(0).And.BeLessThan(context.Users.Count());
        }

        [Fact]
        public async Task FilterByNameTest()
        {
            // setup
            var dbitem = context.Users.First();
            var filter = dbitem.Name[5..10];

            // action
            var result = await service.GetCountAsync(filter);

            // asserts
            result.Should().NotBe(0).And.BeLessThan(context.Users.Count());
        }
    }
}

﻿using Newtonsoft.Json;

namespace SPR.IdentityServer.Api.DataContract.Users
{
    public class ChangePasswordRequest
    {
        /// <summary>
        /// New user's password. Allow null
        /// </summary>
        [JsonProperty(Required = Required.AllowNull)]
        public string? Password { get; set; }
    }
}

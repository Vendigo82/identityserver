﻿using IdentityServer4.Validation;
using SPR.IdentityServer.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Services
{
    public class ResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
    {
        private readonly IUserCredentialsValidator _validator;

        public ResourceOwnerPasswordValidator(IUserCredentialsValidator validator)
        {
            _validator = validator ?? throw new ArgumentNullException(nameof(validator));
        }

        public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            var (result, model) = await _validator.ValidateCredentialsAsync(context.UserName, context.Password, null);
            if (result == ValidateResult.Success)
                context.Result = new GrantValidationResult(model.Id.ToString(), context.Request.GrantType);
            else
                context.Result = new GrantValidationResult(IdentityServer4.Models.TokenRequestErrors.InvalidGrant, "User with given login and password does not found");            
        }
    }
}

﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using SPR.IdentittyServer.IntegrationTests.SwaggerClient;
using SPR.IdentityServer.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.IntegrationTests
{
    public class UsersTests : BaseTests
    {
        public UsersTests(WebServiceTestFixture fixture) : base(fixture)
        {
        }

        [Fact]
        public async Task GetListTest()
        {
            // setup

            // action
            var list = await fixture.Client.ApiUsersGetAsync();

            // asserts
            list.Should().NotBeNull();
            list.Items.Should().NotBeNullOrEmpty();
        }

        [Fact]
        public async Task GetUserTest()
        {
            // setup
            var dbuser = Context.Users.First();

            // action
            var user = await fixture.Client.ApiUsersGetAsync(dbuser.Id);

            // asserts
            user.Should().NotBeNull();
            dbuser.Should().BeEquivalentTo(user, o => o.Excluding(i => i.LoginType));
        }

        [Fact]
        public async Task GetCountTest()
        {
            // setup

            // action
            var response = await fixture.Client.ApiUsersCountAsync();

            // asserts
            response.Count.Should().Be(Context.Users.Count());
        }

        [Theory, AutoData]
        public async Task CrudTest(UserDto userInsert, UserDto userUpdate)
        {
            // setup
            userUpdate.Id = userInsert.Id;
            createdUsers.Add(userInsert.Id);

            // action
            var insertedId = await fixture.Client.ApiUsersPutAsync(userInsert);
            createdUsers.Add(insertedId.Id);
            var afterInsert = await fixture.Client.ApiUsersGetAsync(insertedId.Id);
            await fixture.Client.ApiUsersPostAsync(userUpdate);
            var afterUpdate = await fixture.Client.ApiUsersGetAsync(insertedId.Id);

            // asserts
            insertedId.Id.Should().Be(userInsert.Id);
            afterInsert.Should().BeEquivalentTo(userInsert);
            afterUpdate.Should().BeEquivalentTo(userUpdate);
        }

        [Fact]
        public async Task GetUserClientsTest()
        {
            // setup
            var dbuser = Context.Users.First();

            // action
            var response = await fixture.Client.ApiUsersClientsGetAsync(dbuser.Id);

            // asserts
            response.Should().NotBeNull();
            response.Items.Should().NotBeNull();
        }

        [Fact]
        public async Task SetUserClientsTest()
        {
            // setup
            var dbuser = Context.Users.First();

            // action
            await fixture.Client.ApiUsersClientsPostAsync(dbuser.Id, new UpdateUserClientsRequest { });
        }

        [Theory, AutoData]
        public async Task ChangePasswordTest(string password)
        {
            // setup
            var dbuser = new User { Login = Guid.NewGuid().ToString(), LoginTypeId = 1 };
            Context.Users.Add(dbuser);
            await Context.SaveChangesAsync();
            createdUsers.Add(dbuser.Id);

            // asserts
            await fixture.Client.ApiUsersPasswordAsync(dbuser.Id, new ChangePasswordRequest { Password = password });

            // asserts
            await Context.Entry(dbuser).ReloadAsync();
            dbuser.Salt.Should().NotBeNullOrWhiteSpace();
            dbuser.PasswordHash.Should().NotBeNullOrWhiteSpace();
        }

        [Theory, AutoData]
        public async Task IdempotentInsertTest(UserDto user)
        {
            // setup
            createdUsers.Add(user.Id);

            // action
            var responsePut1 = await fixture.Client.ApiUsersPutAsync(user);
            var responsePut2 = await fixture.Client.ApiUsersPutAsync(user);

            // asserts
            responsePut1.Id.Should().Be(user.Id);
            responsePut2.Id.Should().Be(user.Id);
        }

        [Theory, AutoData]
        public async Task LoginViolationTest(UserDto user1, UserDto user2)
        {
            // setup
            createdUsers.Add(user1.Id);
            createdUsers.Add(user2.Id);
            user2.Login = user1.Login;

            // action
            var responsePut1 = await fixture.Client.ApiUsersPutAsync(user1);
            Func<Task> action = () => fixture.Client.ApiUsersPutAsync(user2);

            // asserts
            (await action.Should().ThrowExactlyAsync<ApiException<ProblemDetails>>()).Which.StatusCode.Should().Be(409);
        }

        [Fact]
        public async Task GetUsersByIdTest()
        {
            // setup
            var dbuser = await Context.Users.FirstAsync();

            // action
            var response = await fixture.HttpClient.GetAsync($"api/users/bunch?id={dbuser.Id}");

            // asserts
            response.Should().Be200Ok().And.BeAs(new { 
                items = new[] { 
                    new { 
                        id = dbuser.Id,
                        isDisable = dbuser.IsDisabled,
                        name = dbuser.Name,
                        login = dbuser.Login
                    }
                }
            });

        }
    }
}

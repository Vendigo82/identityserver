﻿using AutoFixture.Xunit2;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Services.Auth.Tests
{
    public class SHA256PasswordHasherTests
    {
        readonly SHA256PasswordHasher hasher = new();

        [Theory, AutoData]
        public void Test(string salt, string password)
        {
            // action
            var result = hasher.HashPassword(salt, password);

            // asserts
            result.Should().NotBeNullOrEmpty();
        }
    }
}

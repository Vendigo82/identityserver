﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace SPR.IdentityServer.Api.DataContract
{
    public class ListContainer<T>
    {
        [JsonProperty(Required = Required.Always)]
        public IEnumerable<T> Items { get; init; } = null!;
    }
}

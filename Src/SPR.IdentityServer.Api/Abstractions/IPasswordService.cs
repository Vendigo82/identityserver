﻿using System;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Api.Abstractions
{
    public interface IPasswordService
    {
        /// <summary>
        /// Setp password for user
        /// </summary>
        /// <param name="userId">User's id</param>
        /// <param name="password">new password. Can be null</param>
        /// <returns></returns>
        /// <exception cref="Exceptions.UserNotFoundException">If user not found</exception>
        /// <exception cref="Exceptions.ChangePasswordViolationException">If user's login type is domain</exception>
        public Task SetPasswordAsync(Guid userId, string? password);

        /// <summary>
        /// Verify and set new password for user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="password"></param>
        /// <param name="oldPassword"></param>
        /// <returns></returns>
        public Task SetPasswordAsync(Guid userId, string? password, string? oldPassword);
    }
}

﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using SPR.IdentityServer.Api.Abstractions;
using SPR.IdentityServer.Api.DataContract.Clients;
using SPR.IdentityServer.Api.DataContract.Users;
using SPR.IdentityServer.Api.Exceptions;
using SPR.IdentityServer.Api.Extensions;
using SPR.IdentityServer.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Api.Services
{
    public class UsersCRUDService : IUsersCRUDService
    {
        private readonly IMapper _mapper;
        private readonly UsersContext _context;

        public UsersCRUDService(UsersContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<IEnumerable<UserDto>> GetListAsync(int offset, int count, string? filter)
        {
            var q = _context.Users.AsQueryable().AsNoTracking();
            q = ApplyFilter(q, filter);

            return await q.OrderBy(i => i.Login)
                .Skip(offset)
                .Take(count)
                .ProjectTo<UserDto>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<IEnumerable<UserDto>> GetListAsync(IEnumerable<Guid> ids) => await _context.Users.AsNoTracking()
            .Where(i => ids.Contains(i.Id))
            .ProjectTo<UserDto>(_mapper.ConfigurationProvider)
            .ToListAsync();

        public Task<UserDto?> GetAsync(Guid id) => _context.Users.AsNoTracking()
            .Where(i => i.Id == id)
            .ProjectTo<UserDto?>(_mapper.ConfigurationProvider)
            .SingleOrDefaultAsync();

        public Task<int> GetCountAsync(string? filter) => ApplyFilter(_context.Users, filter).CountAsync();

        public async Task<(Guid Id, bool Inserted)> InsertAsync(UserDto user)
        {
            var item = _mapper.Map<User>(user);
            await _context.Users.AddAsync(item);

            try {
                await _context.SaveChangesAsync();
            } catch (DbUpdateException e) {
                if (e.IsPrimaryKeyViolation(out var _))
                    return (user.Id, false);
                else if (e.IsUniqueKeyViolation(out string msg, out string constraintName))
                    throw new ConstraintViolationException($"There are exists another user same value for '{constraintName}': {msg}", constraintName, e);
                else
                    throw;
            }

            return (item.Id, true);
        }

        public async Task UpdateAsync(UserDto user)
        {
            var item = await _context.Users.FirstOrDefaultAsync(i => i.Id == user.Id);
            if (item == null)
                throw new UserNotFoundException(user.Id);

            _mapper.Map(user, item);

            try {
                var cnt = await _context.SaveChangesAsync();
            } catch (DbUpdateException e) {
                if (e.IsUniqueKeyViolation(out var msg, out var columnName))
                    throw new ConstraintViolationException($"There are exists another user same value for '{columnName}': {msg}", columnName, e);
                else
                    throw;
            }
        }

        public async Task UpdateAsync(Guid userId, UserProfileUpdateDto user)
        {
            var item = await _context.Users.FirstOrDefaultAsync(i => i.Id == userId);
            if (item == null)
                throw new UserNotFoundException(userId);

            item.Name = user.Name;

            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<ClientDto>> GetAuthorizedClientsAsync(Guid userId)
        {
            return await _context
                .UserClients
                .AsNoTracking()
                .Where(i => i.UserId == userId)
                .Select(i => i.Client)
                .OrderBy(i => i.SystemName)
                .ProjectTo<ClientDto>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task UpdateAuthorizedClientsAsync(Guid userId, IEnumerable<Guid> revokeClients, IEnumerable<Guid> grantClients)
        {
            var items = await _context.UserClients.Where(i => i.UserId == userId).ToArrayAsync();

            foreach (var client in revokeClients) {
                var record = items.FirstOrDefault(i => i.ClientId == client);
                if (record != null)
                    _context.Entry(record).State = EntityState.Deleted;
            }

            foreach (var client in grantClients) {
                var record = items.FirstOrDefault(i => i.ClientId == client);
                if (record == null)
                    _context.UserClients.Add(new UserClient { UserId = userId, ClientId = client });
            }

            await _context.SaveChangesAsync();
        }

        private static IQueryable<User> ApplyFilter(IQueryable<User> query, string? filter)
        {
            if (!string.IsNullOrEmpty(filter))
                return query.Where(i => i.Login.Contains(filter) || (i.Name != null && i.Name.Contains(filter)));
            else
                return query;
        }
    }
}

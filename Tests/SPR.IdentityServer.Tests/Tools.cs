﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using SPR.IdentityServer.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Tests
{
    public static class Tools
    {
        public static UsersContext InitMemoryDd()
        {
            var name = Guid.NewGuid().ToString();

            var optBuilder = new DbContextOptionsBuilder<UsersContext>()
                .UseInMemoryDatabase(name);
                //.ConfigureWarnings(w => w.Ignore(InMemoryEventId.TransactionIgnoredWarning));
            return new UsersContext(optBuilder.Options);
        }
    }
}

﻿using AutoFixture.Xunit2;
using FluentAssertions.AspNetCore.Mvc;
using Moq;
using SPR.IdentityServer.Api.DataContract.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Controllers.LocalApi.UsersControllerTests
{
    public class InsertTests : UsersControllerBaseTests
    {
        [Theory]
        [InlineAutoData(true)]
        [InlineAutoData(false)]
        public async Task SuccessTest(bool emptyId, UserDto user)
        {
            // setup
            var id = user.Id;
            serviceMock.Setup(f => f.InsertAsync(user)).ReturnsAsync((user.Id, true));

            if (emptyId)
                user.Id = Guid.Empty;

            // action
            var result = await controller.InsertUserAsync(user);

            // asserts
            result.Should()
                .BeCreatedAtActionResult()
                .WithActionName(UsersController.GetUserByIdActionName)
                .WithRouteValue("id", id)
                .WithValueEquivalentTo(new { Id = id });
        }
    }
}

﻿using SPR.IdentityServer.Abstractions.Auth;
using System;

namespace SPR.IdentityServer.Services.Auth
{
    public class PasswordValidator : IPasswordValidator
    {
        readonly IPasswordHasher hasher;

        public PasswordValidator(IPasswordHasher hasher)
        {
            this.hasher = hasher ?? throw new ArgumentNullException(nameof(hasher));
        }

        public bool Validate(string? salt, string? passwordHash, string? password)
        {
            if (string.IsNullOrEmpty(passwordHash))
                return string.IsNullOrEmpty(password);
            else {
                var hash = hasher.HashPassword(salt ?? string.Empty, password ?? string.Empty);
                return hash == passwordHash;
            }
        }
    }
}

﻿using AutoFixture;
using FluentAssertions;
using SPR.IdentityServer.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Api.Services.UsersCRUDServiceTests
{
    public class GetAuthorizedClientsTests : UsersCRUDServiceBaseTests
    {
        public GetAuthorizedClientsTests()
        {
            context.Users.AddRange(Enumerable.Range(1, 2).Select(i => fixture.Create<User>()));
            context.Clients.AddRange(Enumerable.Range(1, 3).Select(i => fixture.Create<Client>()));
            context.SaveChanges();

            var user = context.Users.First();            
            foreach (var c in context.Clients.Take(2))
                user.UserClients.Add(new UserClient { UserId = user.Id, ClientId = c.Id });

            context.SaveChanges();
        }

        [Fact]
        public async Task GetNonEmptyListTest()
        {
            // setup
            var dbitem = context.Users.First(i => i.UserClients.Any());

            // action
            var result = await service.GetAuthorizedClientsAsync(dbitem.Id);

            // asserts
            result.Should().NotBeEmpty().And.BeEquivalentTo(dbitem.UserClients.Select(i => i.Client), o => o
                .Excluding(i => i.UserClients)
                .Excluding(i => i.Configuration));
        }

        [Fact]
        public async Task GetEmptyListTest()
        {
            // setup
            var dbitem = context.Users.First(i => i.UserClients.Any() == false);

            // action
            var result = await service.GetAuthorizedClientsAsync(dbitem.Id);

            // asserts
            result.Should().BeEmpty();
        }
    }
}

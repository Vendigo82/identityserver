﻿using Moq;
using FluentAssertions.AspNetCore.Mvc;
using SPR.IdentityServer.Api.DataContract.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using AutoFixture.Xunit2;

namespace SPR.IdentityServer.Controllers.LocalApi.UsersControllerTests
{
    public class UpdateUserClientsTests : UsersControllerBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(Guid userId, UpdateUserClientsRequest request)
        {
            // setup

            // action
            var result = await controller.UpdateUserClientsAsync(userId, request);

            // asserts
            result.Should().BeOkResult();
            serviceMock.Verify(f => f.UpdateAuthorizedClientsAsync(userId, request.Revoke, request.Grant), Times.Once);
        }

        [Theory, AutoData]
        public async Task WithNullListsTest(Guid userId)
        {
            // setup
            var request = new UpdateUserClientsRequest { };

            // action
            var result = await controller.UpdateUserClientsAsync(userId, request);

            // asserts
            result.Should().BeOkResult();
            serviceMock.Verify(f => f.UpdateAuthorizedClientsAsync(userId, Enumerable.Empty<Guid>(), Enumerable.Empty<Guid>()), Times.Once);
        }
    }
}

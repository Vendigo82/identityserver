﻿using AutoFixture;
using AutoMapper;
using SPR.IdentityServer.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Api.Services.UsersCRUDServiceTests
{
    public class UsersCRUDServiceBaseTests
    {
        protected readonly Fixture fixture = new Fixture();
        protected readonly UsersContext context;
        protected readonly UsersCRUDService service;

        public UsersCRUDServiceBaseTests()
        {
            fixture.Register<User>(() => fixture
                .Build<User>()
                .With(i => i.UserClients, () => new List<UserClient>())
                .Without(i => i.LoginType)
                .With(i => i.LoginTypeId, () => fixture.Create<short>() % 2 + 1)
                .Create()
                );

            fixture.Register<Client>(() => fixture
                .Build<Client>()
                .With(i => i.UserClients, () => new List<UserClient>())
                .Create()
                );

            context = Tools.InitMemoryDd();

            var mapper = new Mapper(new MapperConfiguration(c => c.AddProfile<Mapping.MappingProfile>()));

            service = new UsersCRUDService(context, mapper);
        }
    }
}
